//import io.confluent.kafka.schemaregistry.client.{CachedSchemaRegistryClient, SchemaRegistryClient}
//import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer
//import org.apache.avro.Schema
//import org.apache.avro.generic.GenericRecord
//import org.apache.spark.sql.SparkSession
//import org.apache.spark.sql.avro.SchemaConverters
//import org.apache.spark.sql.functions._
//import org.apache.spark.sql.avro.to_avro
//
//object Join {
//  private val schemaRegistryUrl = "http://127.0.0.1:30081"
//  private val kafkaUrl = "kafka.cluster.local:31090"
//
//  private val schemaRegistryClient = new CachedSchemaRegistryClient(schemaRegistryUrl, 128)
//  private val kafkaAvroDeserializer = new AvroDeserializer(schemaRegistryClient)
//
//
//
//  def main(args: Array[String]): Unit = {
//    val spark = SparkSession
//      .builder
//      .appName("ConfluentConsumer")
//      .master("local[*]")
//      .getOrCreate()
//
//    spark.sparkContext.setLogLevel("ERROR")
//
//    spark.udf.register("deserialize", (bytes: Array[Byte]) =>
//      DeserializerWrapper.deserializer.deserialize(bytes)
//    )
//
//    val purchaseTopic = spark
//      .readStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaUrl)
//      .option("subscribe", "purchase")
//      .option("startingOffsets", "earliest") // From starting
//      .load()
//
//    val purchaseDataFrame = purchaseTopic.selectExpr("""deserialize(value) AS message""")
//
//    val purchaseSchema = schemaRegistryClient.getLatestSchemaMetadata("purchase" + "-value").getSchema
//    val purchaseSchemaSql = SchemaConverters.toSqlType(new Schema.Parser().parse(purchaseSchema))
//
//    val purchaseformattedDataFrame = purchaseDataFrame.select(
//      from_json(col("message"), purchaseSchemaSql.dataType).alias("purchase"))
//      .select("purchase.*")
//
//
//    val customerTopic = spark
//      .readStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaUrl)
//      .option("subscribe", "customer")
//      .option("startingOffsets", "earliest") // From starting
//      .load()
//
//    val customerDataFrame = customerTopic.selectExpr("""deserialize(value) AS message""")
//
//    val customerSchema = schemaRegistryClient.getLatestSchemaMetadata("customer" + "-value").getSchema
//    val customerSchemaSql = SchemaConverters.toSqlType(new Schema.Parser().parse(customerSchema))
//
//    val customerformattedDataFrame = customerDataFrame.select(
//      from_json(col("message"), customerSchemaSql.dataType).alias("customer"))
//      .select("customer.*")
//
//
//    val innerJoinDf = purchaseformattedDataFrame.join(customerformattedDataFrame,"user_id")
//
//    innerJoinDf
//      .writeStream
//      .format("console")
//      .option("truncate", false)
//      .start()
//      .awaitTermination()
//
//    innerJoinDf.select(to_avro(struct("value")) as "value")
//      .writeStream
//      .format("kafka")
//      .outputMode("append")
//      .option("kafka.bootstrap.servers", "kafka.cluster.local")
//      .option("topic", "sparkJoin")
//      .option("checkpointLocation","c:/tmp")
//      .start()
//      .awaitTermination()
//
//
//  }
//
//  object DeserializerWrapper {
//    val deserializer = kafkaAvroDeserializer
//  }
//
//  class AvroDeserializer extends AbstractKafkaAvroDeserializer {
//    def this(client: SchemaRegistryClient) {
//      this()
//      this.schemaRegistry = client
//    }
//
//    override def deserialize(bytes: Array[Byte]): String = {
//      val genericRecord = super.deserialize(bytes).asInstanceOf[GenericRecord]
//      genericRecord.toString
//    }
//  }
//
//}