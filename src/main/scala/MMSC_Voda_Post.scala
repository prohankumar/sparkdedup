import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Get, Result, Table}
import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, lit, trim, udf}
import org.apache.spark.sql.types.{FloatType, IntegerType, StringType}


object MMSC_Voda_Post {
  case class InputRow(callType: String, startTime: String, teleServiceCode: String, bearerServiceCode: String, volumeVolume: String, volumeUmcode: String, spNumberAddress: String, spNumberNumberingPlan: String, spNumberTypeOfNumber: String, spNumberClir: String, spNumberNetwork: String, spPort: String, spEquipmentNumber: String, spEquipmentClassMark: String, spLocation: String, netElementAddress: String, netElementNetwork: String, opNumberAddress: String, opNumberTypeOfNumber: String, opNumberNumberingPlan: String, RatingGroup: String, routingNumberAddress: String, causeForForward: String, cugInfoCugInterLockCode: String, cugInfoCugInformation: String, cugInfoCugType: String, techInfoTermind: String, techInfoType: String, inTrunk: String, outTrunk: String, recordIdentificationCDRid: String, dataVolumeVolume: String, dataVolumeUmcode: String, messagesVolume: String, messagesUmcode: String, callTypeInfoXfileInd: String, sPNumberingPlan: String, spLocationNumberingPlan: String, routingNumberNumberingPlan: String, netElementNumberingPlan: String, upLinkVolumeVolume: String, downLinkVolumeVolume: String, upLinkVolumeUmcode: String, downLinkVolumeUmcode: String, startTimeDummy: String, servedPDPAddressAddress: String, servedPDPAddressNumberingplan: String, recordIdentificationOrigCdrId: String, qualityOfServiceNegotiatedDelay: String, qualityOfServiceNegotiatedPeakThroughput: String, qualityOfserviceNegotiatedMeanThroughput: String, qualityOfserviceNegotiatedPrecedence: String, qualityOfserviceNegotiatedReliability: String, sgsnAddressSgsnAddress: String, netElementEntity: String, netElementAddressNew: String, netElementNumberingPlanNew: String, callTypeInfoNetworkInitContextId: String, technicalInfoAnonymousAccess: String, Duration: String, CellID: String, TimeZone: String, rATType: String, GGSN_Address: String, GPRSEndTime: String, CauseForRecClosing: String, NodeId: String, SubscriberType: String, ContentType: String, ServiceName: String, f3gpp_Qos_Profile: String, EffectiveDuration: String, PDP_Type: String, Selection_Mode: String, TimeOfFirstUsage: String, TimeOfLastUsage: String, DataPacketDownlink: String, DataPacketUplink: String, LocalSequence: String, DataVolDownlinkTTC: String, GGSN_MCC_MNC: String, AcctSessionID: String, Roaming: String,Delimiter: String, SRC_FILE_NAME: String, validity: String, inputRow: String)


  def main(args: Array[String]): Unit = {
    val tcmIMSICodeSeriesLookup = "tcmIMSICodeSeriesLookup"
    val npdbMSISDNLookup = "npdbMSISDNLookup"
    val tcmLrnLookup = "tcmLrnLookup"
    val tcmOperatorCodeLookup = "tcmOperatorCodeLookup"
    val tcmLrnCircle = "tcmLrnCircle"
    val tcmOperatorCodeCircle = "tcmOperatorCodeCircle"
    val callTypeCode = "callTypeCode"




    if (args.length < 6) {
      print("Arguments not properly specified: [kafkaBrokers, topic, deduplicationInterval, checkpointPath, outputPath, batchSize, SourceCircleDimLocation, DiscoverDeliveryBearerDimLocation, SubsStatusDimLocation, contentTypeTable, productTypeTable]")
      System.exit(400)
    }

    //Command Line Args ************************************************************************
    val kafkaBrokers = args(0)
    val topic = args(1)
//    val watermark = args(2)
//    val checkpointPath = args(3)
//    val outputPath = args(4)
//    val batchSize = args(5)
//    val SourceCircleTable = args(6)
//    val DiscoverDeliveryBearerTable = args(7)
//    val SubsStatusTable = args(8)
//    val contentTypeTable = args(9)
//    val productTypeTable = args(10)
//    val rejectTopic = args(11)
    val security = args(2)
    val writePath = args(3)
    val zkUrl = args(4)
    val checkpointLocation = args(5)
    //******************************************************************************************


    object HbaseObject{
      val table = new HbaseUtilities(zkUrl)
      table.getTable(tcmLrnLookup)
      table.getTable(tcmOperatorCodeLookup)
      table.getTable(tcmIMSICodeSeriesLookup)
      table.getTable(npdbMSISDNLookup)
      table.getTable(tcmLrnCircle)
      table.getTable(tcmOperatorCodeCircle)
      table.getTable(callTypeCode)
    }

    val spark = SparkSession.builder().master("local[*]").appName("MMSC_Voda_Post").getOrCreate()

    spark.sparkContext.setLogLevel("WARN")

    import spark.implicits._

//    val conf: Configuration = HBaseConfiguration.create
//
//    conf.addResource("core-site.xml")
//    conf.addResource("hbase-site.xml")
//    conf.addResource("hdfs-site.xml")


    //    UDFs.................................................................................................................................................................................

    val SUBS_CIRCLE_ID = udf((spPort: String, SUBS_MSISDN: String) => {
      var subsCircleId:Int = -99

      if (spPort != null) {
        if (!spPort.startsWith("404") && !spPort.startsWith("405")) {
          subsCircleId = 100
        } else {
          val operatorCircle = HbaseObject.table.scanRowBestMatch(tcmIMSICodeSeriesLookup, spPort, "cf", "operator_circle")
          subsCircleId = Bytes.toString(operatorCircle).toInt
        }
      } else {
        val lrn = HbaseObject.table.getRow(npdbMSISDNLookup, SUBS_MSISDN, "cf", "route")
        if (lrn != null) {
          val operatorCircle = HbaseObject.table.getRow(tcmLrnLookup, Bytes.toString(lrn), "cf", "operator_circle")
          subsCircleId = Bytes.toString(operatorCircle).toInt
        }
        else {
          val operatorCircle = HbaseObject.table.scanRowBestMatchMulti2(tcmOperatorCodeLookup, SUBS_MSISDN, "cf", "operator_circle")
          subsCircleId = Bytes.toString(operatorCircle).toInt
        }
      }
      subsCircleId
    })


    val SUBS_MSISDN = udf((spNumberAddress: String) => {
      var subsMsisdn: String = null
      if (spNumberAddress.startsWith("91") && spNumberAddress.length>=12) {
        subsMsisdn = spNumberAddress.substring(2)
      } else if (spNumberAddress.startsWith("091") && spNumberAddress.length>=13) {
        subsMsisdn = spNumberAddress.substring(3)
      }
      //      else {
      //        "00" + spNumberAddress
      //      }
      subsMsisdn
    })


    val EVENT_START_DATE = udf((startTime: String) => {
      val dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
      val rtf = DateTimeFormatter.ofPattern("yyyy-MM-dd")
      try {
        rtf.format(dtf.parse(startTime))
      }
      catch {
        case e: java.lang.ArrayIndexOutOfBoundsException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        }
        case e: java.time.format.DateTimeParseException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        }
      }
    })

    val EVENT_START_TIME = udf((startTime: String) => {
      val dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
      val rtf = DateTimeFormatter.ofPattern("HH:mm:ss")
      try {
        rtf.format(dtf.parse(startTime))
      }
      catch {
        case e: java.lang.ArrayIndexOutOfBoundsException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))
        }
        case e: java.time.format.DateTimeParseException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))
        }
      }
    })


    val EVENT_CLASSIFICATION = udf  ((CALLED_CALLING_LRN: Integer, CALLED_CALLING_NUMBER: String, SUBS_CIRCLE_ID: Int) => {
      var eventClassification: Int = 13
      var value: Array[Byte] = null
      var eventClsfType: Integer = null


      if (CALLED_CALLING_NUMBER.contains("@")) {
        eventClassification = 8
      } else {
        if (CALLED_CALLING_LRN != null) {
          value = HbaseObject.table.getRow(tcmLrnCircle, (CALLED_CALLING_LRN.toString + SUBS_CIRCLE_ID.toString), "cf", "EVENT_CLSF_TYPE")
          if (value != null) {
            eventClsfType = Bytes.toString(value).toInt
          }
        }

        else if (eventClsfType == null) {
          value = HbaseObject.table.scanRowBestMatchMulti(tcmOperatorCodeCircle, CALLED_CALLING_NUMBER ," " + SUBS_CIRCLE_ID.toString, "cf", "EVENT_CLSF_TYPE")
          if (value != null) {
            eventClsfType = Bytes.toString(value).toInt
          }
        }


        if (eventClsfType == null) {
          eventClassification = 13
        } else {
          if (eventClsfType == 2) {
            eventClassification = 8
          } else if (eventClsfType == 1) {
            eventClassification = 3
          } else if (eventClsfType == 3) {
            eventClassification = 7
          } else if (eventClsfType == 4) {
            eventClassification = 1
          } else if (eventClsfType == 5) {
            eventClassification = 2
          }
        }
      }


      eventClassification
    })


    val PRE_POST_IND = udf((fileName: String) => {
      var ppi: Integer = null
      if (fileName(5).toString.equals("P")) {
        ppi = 2
      }
      else if (fileName(5).toString.equals("R")) {
        ppi = 1
      }
      ppi
    })


    val CALLED_CALLING_NUMBER = udf((opNumberAddress: String) => {
      var calledCallingNumber: String = opNumberAddress
      if (!opNumberAddress.contains("@")) {
        if (opNumberAddress.startsWith("91") && opNumberAddress.length>=12) {
          calledCallingNumber = opNumberAddress.substring(2)
        } else if (opNumberAddress.startsWith("091") && opNumberAddress.length>=13) {
          calledCallingNumber = opNumberAddress.substring(3)
        }
      }
      calledCallingNumber
    })


    val SOURCE_NETWORK_OPERATOR_KEY = udf((CALLED_CALLING_NUMBER: String, CALLED_CALLING_LRN: String, SUBS_CIRCLE_ID: Int) => {
      var uniqueTrafficCodeID: Integer = -99
      if (CALLED_CALLING_NUMBER.contains("@")) {
        uniqueTrafficCodeID = -99
      } else {
        if (CALLED_CALLING_LRN != null && CALLED_CALLING_LRN != "") {
          val value = HbaseObject.table.getRow(tcmLrnCircle, CALLED_CALLING_LRN + SUBS_CIRCLE_ID, "cf", "UNIQUE_TRAFFIC_CODE_ID")
          if (value != null) {
            uniqueTrafficCodeID = Bytes.toString(value).toInt
          }
        } else {

          val value = HbaseObject.table.scanRowBestMatchMulti(tcmOperatorCodeCircle, CALLED_CALLING_NUMBER, " " + SUBS_CIRCLE_ID.toString, "cf", "UNIQUE_TRAFFIC_CODE_ID")
          if (value != null) {
            uniqueTrafficCodeID = Bytes.toString(value).toInt
          }
        }
      }
      uniqueTrafficCodeID
    })


    val SUBS_MSISDN_LRN = udf((SUBS_MSISDN: String) => {
      var susbMsisdnLrn: Integer = null
      if (SUBS_MSISDN.length == 10) {
        val lrn = HbaseObject.table.getRow(npdbMSISDNLookup, SUBS_MSISDN, "cf", "route")
        if (lrn != null) {
          susbMsisdnLrn = Bytes.toString(lrn).toInt
        }
      }
      else {
        susbMsisdnLrn = null
      }
      susbMsisdnLrn
    })


    val CALLED_CALLING_LRN = udf((CALLED_CALLING_NUMBER: String) => {
      var calledCallingLrn: Integer = null
      if (CALLED_CALLING_NUMBER.length() == 10) {
        val lrn = HbaseObject.table.getRow(npdbMSISDNLookup, CALLED_CALLING_NUMBER, "cf", "route")
        if (lrn != null) {
          calledCallingLrn = Bytes.toString(lrn).toInt
        }
      }
      else {
        calledCallingLrn = null
      }
      calledCallingLrn
    })


    val SUBS_MSISDN_PORT_FLAG = udf((SUBS_IMSI: String, SUBS_MSISDN: String, SUBS_MSISDN_LRN: Integer) => {
      var subsMsidnPortFlag: Int = 0

      val value1 = HbaseObject.table.scanRowBestMatchMulti2(tcmOperatorCodeLookup, SUBS_MSISDN, "cf", "operator_name")
      var value2: Array[Byte] = null
      if (SUBS_MSISDN_LRN != null) {
        value2 = HbaseObject.table.getRow(tcmLrnLookup, SUBS_MSISDN_LRN.toString, "cf", "operator_name")
      }

      if (value2 == null) {
        value2 = HbaseObject.table.scanRowBestMatch(tcmIMSICodeSeriesLookup, SUBS_IMSI, "cf", "operator_name")
      }
      val operatorName1 = Bytes.toString(value1)
      var operatorName2:String = null
      if (value2 != null) {
        operatorName2 = Bytes.toString(value2)
      }

      if (operatorName1 != null) {
        if (!(operatorName1.equalsIgnoreCase("Idea") || operatorName1.equalsIgnoreCase("VEL"))) {
          if (operatorName2 != null && (operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
            subsMsidnPortFlag = 1
          }
        } else {
          if (operatorName2 != null && !(operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
            subsMsidnPortFlag = 2
          }
        }
      }
      subsMsidnPortFlag
    })


    val CALLED_CALLING_PORT_FLAG = udf((CALLED_CALLING_LRN: Integer, CALLED_CALLING_NUMBER: String) => {
      var calledCallingPortFlag: Int = 0

      //    TODO reject records when CALLED_CALLING_NUMBER is null
      if (CALLED_CALLING_NUMBER!= null && !CALLED_CALLING_NUMBER.contains("@")) {
        val value1 = HbaseObject.table.scanRowBestMatchMulti2(tcmOperatorCodeLookup, CALLED_CALLING_NUMBER, "cf", "operator_name")
        var value2: Array[Byte] = null
        if (CALLED_CALLING_LRN != null) {
          value2 = HbaseObject.table.getRow(tcmLrnLookup, CALLED_CALLING_LRN.toString, "cf", "operator_name")
        }

        var operatorName1:String = null
        var operatorName2:String = null
        if (value1 != null) {
          operatorName1 = Bytes.toString(value1)
        }
        if (value2 != null) {
          operatorName2 = Bytes.toString(value2)
        }

        if (operatorName1 != null) {
          if (!(operatorName1.equalsIgnoreCase("Idea") || operatorName1.equalsIgnoreCase("VEL"))) {
            if (operatorName2 != null && (operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
              calledCallingPortFlag = 1
            }
          } else {
            if (operatorName2 != null && !(operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
              calledCallingPortFlag = 2
            }
          }
        }
      }
      calledCallingPortFlag
    })


    val USER_TYPE_CD = udf((callTypeInfoXfileInd: String) => {
      var userTypeCd: String = null
      if (callTypeInfoXfileInd.equals("H")) {
        userTypeCd = "0"
      }
      else if (callTypeInfoXfileInd.equals("V")) {
        userTypeCd = "2"
      }
      userTypeCd
    })


    //    TODO catch exception
    val CALL_GROSS_CHG_AMT = udf((rupees: java.lang.Double) => {
      var callGrossChgAmt: java.lang.Double = null
      try {
        callGrossChgAmt = (rupees / 100.00)
      } catch {
        case e: Exception => println("")
      }
      callGrossChgAmt
    })


    val OFFERING_ID_SVC = udf((fileName: String, bearerServiceCode: Int) => {
      var offeringIDSvc: String = null
      if (fileName(5).toString.equals("R") && bearerServiceCode == 0) {
        offeringIDSvc = "114"
      }
      else {
        offeringIDSvc = null
      }
      offeringIDSvc
    })

    val REPORTED_DATE = udf((fileName: String) => {
      val dtf = DateTimeFormatter.ofPattern("yyyyMMdd")
      val rtf = DateTimeFormatter.ofPattern("yyyy-MM-dd")
      try {
        rtf.format(dtf.parse(fileName.substring(11, 19)))
      }
      catch {
        case e: java.lang.ArrayIndexOutOfBoundsException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        }
        case e: java.time.format.DateTimeParseException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        }
      }
    })

    val LOAD_TIME_STAMP = udf(() => {
      LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    })

    val CALL_TYPE = udf((SRC_FILE_NAME: String, SRC_CALL_TYPE: String) => {
      var callType: String = SRC_CALL_TYPE
      val circleCode = SRC_FILE_NAME.substring(2,5)
      val value = HbaseObject.table.getRow(callTypeCode, circleCode, "cf", "CALL_TYPE_CODE")
      if (value != null) {
        callType = Bytes.toString(value)
      }
      callType
    })

    val BRAND_ID = udf((SRC_FILE_NAME: String) =>{
      if (SRC_FILE_NAME.startsWith("MMSPSM")) {
        1
      } else if (SRC_FILE_NAME.startsWith("MM") && (SRC_FILE_NAME(5).toString == "P" || SRC_FILE_NAME(5).toString == "R")) {
        2
      } else {
        3
      }
    })

    //    END UDFs.................................................................................................................................................................................


    val inputDF = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaBrokers)
      .option("security.protocol", security)
      .option("startingOffsets", "earliest")
      .option("subscribe", topic)
      .load().selectExpr("CAST(value AS STRING)", "CAST(key AS STRING)")

    val parsedDF = inputDF.map( x => {
//      val row = x.getString(0).split("\\|(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1).map(_.trim)
      val row = x.getString(0).split(",", -1).map(_.trim)

//      val fileName = x.getString(1)

      try
        {
          InputRow(row(0), row(1), row(2), row(3), row(4), row(5), row(6), row(7), row(8), row(9), row(10), row(11), row(12), row(13), row(14), row(15), row(16), row(17), row(18), row(19), row(20), row(21), row(22), row(23), row(24), row(25), row(26), row(27), row(28), row(29), row(30), row(31), row(32), row(33), row(34), row(35), row(36), row(37), row(38), row(39), row(40), row(41), row(42), row(43), row(44), row(45), row(46), row(47), row(48), row(49), row(50), row(51), row(52), row(53), row(54), row(55), row(56), row(57), row(58), row(59), row(60), row(61), row(62), row(63), row(64), row(65), row(66), row(67), row(68), row(69), row(70), row(71), row(72), row(73), row(74), row(75), row(76), row(77), row(78), row(79), row(80), row(81), row(82), row(83), row(84), "valid", x.getString(0))
        }
      catch
        {
          case e: java.lang.ArrayIndexOutOfBoundsException => InputRow("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "malformed record", x.getString(0))
        }
    }).toDF()



//    val directlyMappedColumns = Array( ("CONTENT_TYPE", "ContentType"), ("SUBS_IMSI", "spPort"), ("BEARERSERVICECODE", "bearerServiceCode"), ("CALL_TYPE", "callType"), ("TELESERVICECODE", "teleServiceCode"), ("VOLUME_VOLUME", "volumeVolume"), ("VOLUME_UMCODE", "volumeUmcode"), ("SUBS_IMEI", "spEquipmentNumber"), ("S_P_LOCATION", "spLocation"), ("NET_ELEMENT_NETWORK", "netElementNetwork"), ("RECORD_IDENTIFICATION_CDR_ID", "recordIdentificationCDRid"), ("UPLINK_VOLUME", "upLinkVolumeVolume"), ("DOWNLINK_VOLUME", "downLinkVolumeVolume"), ("UPLINK_VOLUME_UMCODE", "upLinkVolumeUmcode"), ("DOWNLINK_VOLUME_UMCODE", "downLinkVolumeUmcode"), ("CALL_DIALED_NUM", "servedPDPAddressAddress"), ("CELL_SITE_ID ", "CellID"), ("SUBS_TYPE", "SubscriberType"), ("SERVICE_NAME", "ServiceName") )
    val directlyMappedColumns = Array( ("CONTENT_TYPE", "ContentType"), ("SUBS_IMSI", "spPort"), ("BEARER_SERVICE_CODE", "bearerServiceCode"), ("CALL_TYPE", "callType"), ("TELE_SERVICE_CODE", "teleServiceCode"), ("VOLUME_VOLUME", "volumeVolume"), ("VOLUME_UMCODE", "volumeUmcode"), ("SUBS_IMEI", "spEquipmentNumber"), ("S_P_LOCATION", "spLocation"), ("NET_ELEMENT_NETWORK", "netElementNetwork"), ("RECORD_IDENTIFICATION_CDR_ID", "recordIdentificationCDRid"), ("UPLINK_VOLUME", "upLinkVolumeVolume"), ("DOWNLINK_VOLUME", "downLinkVolumeVolume"), ("UPLINK_VOLUME_UMCODE", "upLinkVolumeUmcode"), ("DOWNLINK_VOLUME_UMCODE", "downLinkVolumeUmcode"), ("CALL_DIALED_NUM", "servedPDPAddressAddress"), ("CELL_SITE_ID", "CellID"), ("SUBS_TYPE", "SubscriberType"), ("SERVICE_NAME", "ServiceName") )

//    val columnsToType = Array( ("upLinkVolume","long"), ("downLinkVolume","long"), ("CONTENT_TYPE", "integer") )
    val columnsToType = Array( ("UPLINK_VOLUME","long"), ("DOWNLINK_VOLUME","long"), ("UPLINK_VOLUME_UMCODE","integer"), ("DOWNLINK_VOLUME_UMCODE","integer"), ("CONTENT_TYPE", "integer"), ("VOLUME_VOLUME", "integer"), ("VOLUME_UMCODE", "integer"))

    val directRnamedDF = directlyMappedColumns.foldLeft(parsedDF.filter(col("validity") === "valid"))( (tempDF,column) => tempDF.withColumnRenamed(column._2, column._1))


    val directTypedDF = columnsToType.foldLeft(directRnamedDF)( (tempDF,column) => tempDF.withColumn(column._1, col(column._1).cast(column._2)))


//    val currentTimestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))


    val transformedDF = directTypedDF
      .withColumn("SUBS_MSISDN", SUBS_MSISDN(col("spNumberAddress")))
      .withColumn("SUBS_CIRCLE_ID",SUBS_CIRCLE_ID(col("SUBS_IMSI"), col("SUBS_MSISDN")))
      .withColumn("EVENT_START_DATE", EVENT_START_DATE(col("startTime")))
      .withColumn("EVENT_START_TIME", EVENT_START_TIME(col("startTime")))
      .withColumn("EVENT_TYPE_KEY", lit(4))
      .withColumn("CALLED_CALLING_NUMBER", CALLED_CALLING_NUMBER(col("opNumberAddress")))
      .withColumn("CALLED_CALLING_LRN", CALLED_CALLING_LRN(col("CALLED_CALLING_NUMBER")))
      .withColumn("EVENT_CLASSIFICATION", EVENT_CLASSIFICATION(col("CALLED_CALLING_LRN"), col("CALLED_CALLING_NUMBER"), col("SUBS_CIRCLE_ID")))
      .withColumn("EVENT_DIRECTION", lit(2))
      .withColumn("PRE_POST_IND", PRE_POST_IND(col("SRC_FILE_NAME")))
      .withColumn("ROAMING_IND", lit(1))
      .withColumn("BRAND_ID", BRAND_ID(col("SRC_FILE_NAME")))
      .withColumn("SOURCE_NETWORK_OPERATOR_KEY", SOURCE_NETWORK_OPERATOR_KEY(col("CALLED_CALLING_NUMBER"), col("CALLED_CALLING_LRN"), col("SUBS_CIRCLE_ID")))
      .withColumn("SUBS_MSISDN_LRN", SUBS_MSISDN_LRN(col("SUBS_MSISDN")))
      .withColumn("SUBS_MSISDN_PORT_FLAG", SUBS_MSISDN_PORT_FLAG(col("SUBS_IMSI"), col("SUBS_MSISDN"), col("SUBS_MSISDN_LRN")))
      .withColumn("USER_TYPE_CD", USER_TYPE_CD(col("callTypeInfoXfileInd")))
      .withColumn("TOTAL_VOLUME", col("UPLINK_VOLUME") + col("DOWNLINK_VOLUME"))
      .withColumn("REPORTED_DATE", REPORTED_DATE(col("SRC_FILE_NAME")))
      .withColumn("NETWORK_TYPE_CD", lit("2G"))
      .withColumn("CALLED_CALLING_PORT_FLAG", CALLED_CALLING_PORT_FLAG(col("CALLED_CALLING_LRN"), col("CALLED_CALLING_NUMBER")))
      .withColumn("LOAD_TIME_STAMP", LOAD_TIME_STAMP())
      .withColumn("CALL_TYPE", CALL_TYPE(col("SRC_FILE_NAME"), col("CALL_TYPE")))

//      .select("SUBS_CIRCLE_ID", "SUBS_MSISDN", "EVENT_START_DATE", "EVENT_START_TIME", "EVENT_TYPE_KEY", "EVENT_CLASSIFICATION", "EVENT_DIRECTION", "PRE_POST_IND", "ROAMING_IND", "CALLED_CALLING_NUMBER", "SOURCE_NETWORK_OPERATOR_KEY", "CONTENT_TYPE", "SUBS_MSISDN_LRN", "CALLED_CALLING_LRN", "SUBS_IMSI", "SUBS_MSISDN_PORT_FLAG", "CALLED_CALLING_PORT_FLAG", "BEARER_SERVICE_CODE", "CALL_TYPE", "TELE_SERVICE_CODE", "VOLUME_VOLUME", "VOLUME_UMCODE", "SUBS_IMEI", "S_P_LOCATION", "NET_ELEMENT_NETWORK", "RECORD_IDENTIFICATION_CDR_ID", "USER_TYPE_CD", "UPLINK_VOLUME", "DOWNLINK_VOLUME", "UPLINK_VOLUME_UMCODE", "DOWNLINK_VOLUME_UMCODE", "TOTAL_VOLUME", "CALL_DIALED_NUM", "CELL_SITE_ID", "NETWORK_TYPE_CD", "SUBS_TYPE", "SERVICE_NAME", "LOAD_TIME_STAMP", "BRAND_ID", "REPORTED_DATE", "SRC_FILE_NAME")
      //    Idea specific columns

      .withColumn("SENDER_TERMINAL", lit(null).cast(StringType))
      .withColumn("RECIPIENT_TERMINAL", lit(null).cast(StringType))
      .withColumn("MMSC_ID", lit(null).cast(StringType))
      .withColumn("VIDEO_CONTENT_SIZE", lit(null).cast(IntegerType))
      .withColumn("VIDEO_CONTENT_NUMBER", lit(null).cast(IntegerType))
      .withColumn("AUDIO_CONTENT_SIZE", lit(null).cast(IntegerType))
      .withColumn("AUDIO_CONTENT_NUMBER", lit(null).cast(IntegerType))
      .withColumn("IMAGE_CONTENT_SIZE", lit(null).cast(IntegerType))
      .withColumn("IMAGE_CONTENT_NUMBER", lit(null).cast(IntegerType))
      .withColumn("TEXT_CONTENT_SIZE", lit(null).cast(IntegerType))
      .withColumn("TEXT_CONTENT_NUMBER", lit(null).cast(IntegerType))
      .withColumn("OTHER_CONTENT_SIZE", lit(null).cast(IntegerType))
      .withColumn("OTHER_CONTENT_NUMBER", lit(null).cast(IntegerType))
      .withColumn("CALLED_CALLING_IMSI", lit(null).cast(StringType))
      .withColumn("DELIVERY_STATUS", lit(null).cast(StringType))
      .withColumn("S_P_NO_NETWORK", lit(null).cast(StringType))
      .withColumn("CALL_GROSS_CHG_AMT", lit(null).cast(FloatType))
      .withColumn("OFFERING_ID_SVC", lit(null).cast(IntegerType))
      .withColumn("ROAMING_NETWORK_OPERATOR_KEY", lit(null).cast(IntegerType))
      .select("SUBS_CIRCLE_ID", "SUBS_MSISDN", "EVENT_START_DATE", "EVENT_START_TIME", "EVENT_TYPE_KEY", "EVENT_CLASSIFICATION", "EVENT_DIRECTION", "PRE_POST_IND", "ROAMING_IND", "SENDER_TERMINAL", "RECIPIENT_TERMINAL", "CALLED_CALLING_NUMBER", "MMSC_ID", "SOURCE_NETWORK_OPERATOR_KEY", "ROAMING_NETWORK_OPERATOR_KEY", "CONTENT_TYPE", "VIDEO_CONTENT_SIZE", "VIDEO_CONTENT_NUMBER", "AUDIO_CONTENT_SIZE", "AUDIO_CONTENT_NUMBER", "IMAGE_CONTENT_SIZE", "IMAGE_CONTENT_NUMBER", "TEXT_CONTENT_SIZE", "TEXT_CONTENT_NUMBER", "OTHER_CONTENT_SIZE", "OTHER_CONTENT_NUMBER", "SUBS_MSISDN_LRN", "CALLED_CALLING_LRN", "SUBS_IMSI", "CALLED_CALLING_IMSI", "SUBS_MSISDN_PORT_FLAG", "CALLED_CALLING_PORT_FLAG", "DELIVERY_STATUS", "S_P_NO_NETWORK", "BEARER_SERVICE_CODE", "CALL_TYPE", "TELE_SERVICE_CODE", "VOLUME_VOLUME", "VOLUME_UMCODE", "SUBS_IMEI", "S_P_LOCATION", "NET_ELEMENT_NETWORK", "RECORD_IDENTIFICATION_CDR_ID", "USER_TYPE_CD", "UPLINK_VOLUME", "DOWNLINK_VOLUME", "UPLINK_VOLUME_UMCODE", "DOWNLINK_VOLUME_UMCODE", "TOTAL_VOLUME", "CALL_GROSS_CHG_AMT", "CALL_DIALED_NUM", "CELL_SITE_ID", "NETWORK_TYPE_CD", "SUBS_TYPE", "SERVICE_NAME", "OFFERING_ID_SVC", "LOAD_TIME_STAMP", "BRAND_ID", "REPORTED_DATE", "SRC_FILE_NAME")


    transformedDF.writeStream
      .format("orc")
      .option("path", writePath)
      .option("checkpointLocation", checkpointLocation)
      .start()
      .awaitTermination()

//    transformedDF
//      .writeStream
//      .outputMode("update")
//      .format("console")
//      .start()
//      .awaitTermination()



    //    val nonNullablesColumns = Array ("GLOBAL_TRANSACTION_ID", "SERVICE_ID", "CONTENT_PROVIDER_CODE", "SUBS_KEY", "CHARGED_SUBS_MSISDN", "DELIVERY_DATE", "DELIVERY_TIME")
//
//    val nonNullablesFiltered = nonNullablesColumns.foldLeft(transformedDF)(  (tempDF, column) => tempDF.filter(col(column) =!= "").filter(col(column).isNotNull) ).drop("inputRow")
//    val nonNullablesRejected = nonNullablesColumns.foldLeft(transformedDF)(  (tempDF, column) => tempDF.filter(col(column) === "").filter(col(column).isNull).withColumn("validity", lit(column + " is null")) )
//
//
//    nonNullablesFiltered.withWatermark("watermarkDatetime", watermark + " days").dropDuplicates("GLOBAL_TRANSACTION_ID", "watermarkDatetime").drop("watermarkDatetime").repartition(5).writeStream.option("checkpointLocation", checkpointPath).outputMode("append").option("path",outputPath).trigger(Trigger.ProcessingTime(batchSize + " seconds")).format("orc").start()
//
//    val rejectedDF = parsedDF.filter(col("validity") =!= "valid")
//
//    rejectedDF
//      .select( col("SRC_FILE_NAME").alias("key"), to_json(struct("inputRow","validity")).alias("value"))
//      .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
//      .writeStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaBrokers)
//      .option("security.protocol", security)
//      .option("topic", rejectTopic)
//
//    nonNullablesRejected
//      .select( col("SRC_FILE_NAME").alias("key"), to_json(struct("inputRow","validity")).alias("value"))
//      .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
//      .writeStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaBrokers)
//      .option("security.protocol", security)
//      .option("topic", rejectTopic)
//
//    spark.streams.awaitAnyTermination()
  }

}
