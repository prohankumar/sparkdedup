//import org.json.{JSONArray, JSONObject}
//
//import scala.collection.mutable.ListBuffer
//
//
//case class ProcessorConf(cols:List[String],timeColumn:String,delayThreshold:String)
//
//object ProcessorConf extends ProcessorConfPrototype[ProcessorConf] {
//
//  override def apply(confString: String): ProcessorConf = {
//    var metricObj: JSONObject = null
//    try {
//      metricObj = new JSONObject(confString)
//    }
//    catch {
//      case e: org.json.JSONException =>
//        throw invalidConfException("configuration file is not a valid JSON object", e)
//    }
//
//    var idColumns: JSONArray = null
//
//    var cols = new ListBuffer[String]()
//    var timeColumn: String = null
//    var delayThreshold: String = null
//
//    try
//      timeColumn = metricObj.getString("timeColumn")
//    catch {
//      case e: org.json.JSONException =>
//        throw invalidConfException("timeColumn is not in a valid format or does not exist", e)
//    }
//
//
//    try {
//      idColumns = metricObj.getJSONArray("idColumns")
//      idColumns.forEach(c => cols += c.toString)
//    }
//    catch {
//      case e: org.json.JSONException =>
//        throw invalidConfException("idColumns is not in a valid format or does not exist", e)
//    }
//
//    try
//      delayThreshold = metricObj.getString("delayThreshold")
//    catch {
//      case e: org.json.JSONException =>
//        throw invalidConfException("delayThreshold is not in a valid format or does not exist", e)
//    }
//
//
//    ProcessorConf(cols.toList, timeColumn, delayThreshold)
//
//
//  }
//}
