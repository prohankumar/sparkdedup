//import Spark.{AvroDeserializer, DeserializerWrapper, kafkaAvroDeserializer, kafkaUrl, schemaRegistryClient}
//import io.confluent.kafka.schemaregistry.client.{CachedSchemaRegistryClient, SchemaRegistryClient}
//import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer
//import org.apache.avro.Schema
//import org.apache.avro.generic.GenericRecord
//import org.apache.spark.sql.{SparkSession, functions}
//import org.apache.spark.sql.avro.SchemaConverters
//import org.apache.spark.sql.expressions.Window
//import org.apache.spark.sql.functions.{col, from_json, lit, unix_timestamp}
//import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout}
//import org.apache.spark.sql.types.LongType
//
//object DedupWindow {
//
//  private val schemaRegistryUrl = "http://127.0.0.1:30081"
//  private val kafkaUrl = "kafka.cluster.local:31090"
//
//  private val schemaRegistryClient = new CachedSchemaRegistryClient(schemaRegistryUrl, 128)
//  private val kafkaAvroDeserializer = new AvroDeserializer(schemaRegistryClient)
//
//  case class InputRow(user_id:Int, datetime:Long, purchase_value:String, product_type:String, update_ts:java.sql.Timestamp, bucket: java.sql.Timestamp)
//  case class State(data: InputRow, dup: Int)
//
//
//  def main(args: Array[String]): Unit = {
//    val spark = SparkSession
//      .builder
//      .appName("ConfluentConsumer6")
//      .master("local[*]")
//      .getOrCreate()
//
//    spark.sparkContext.setLogLevel("ERROR")
//
//    spark.udf.register("deserialize", (bytes: Array[Byte]) =>
//      DeserializerWrapper.deserializer.deserialize(bytes)
//    )
//
//    val purchase2Topic = spark
//      .readStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaUrl)
//      .option("subscribe", "purchase12")
//      .option("startingOffsets", "earliest") // From starting
//      .load()
//
//    val purchaseDataFrame = purchase2Topic.selectExpr("""deserialize(value) AS message""")
//
//    val purchaseSchema = schemaRegistryClient.getLatestSchemaMetadata("purchase12" + "-value").getSchema
//    val purchaseSchemaSql = SchemaConverters.toSqlType(new Schema.Parser().parse(purchaseSchema))
//
//    val purchaseformattedDataFrame = purchaseDataFrame.select(
//      from_json(col("message"), purchaseSchemaSql.dataType).alias("purchase"))
//      .select("purchase.*")
//
//
//    import spark.implicits._
//
//    def updateStateWithEvent(state:State, input:InputRow):State = {
//
//      if (Option(input.user_id).isEmpty) {
//        return state
//      }
//      if (state.data==null) {
//        State(input, 0)
//      } else {
//        State(input, 1)
//      }
//    }
//
//
//    def updateAcrossEvents(tuple2: Tuple2[Int, java.sql.Timestamp],
//                           inputs: Iterator[InputRow],
//                           oldState: GroupState[State]):State = {
//      //      if (oldState.hasTimedOut) {
//      //
//      //      }
//      var state:State = if (oldState.exists) oldState.get else State(null, 0)
//
//      for (input <- inputs) {
//        state = updateStateWithEvent(state, input)
//        oldState.update(state)
//      }
//      state
//    }
//
//
//    val bucketCol = functions.to_timestamp(col("update_ts").cast(LongType).minus(col("update_ts").cast(LongType).mod(5)))
//
//    purchaseformattedDataFrame
//      .withColumn("bucket", bucketCol)
//      .withWatermark("bucket", "5 seconds")
//      .as[InputRow]
//      .groupByKey(p=>(p.user_id, p.bucket))
//      .mapGroupsWithState(GroupStateTimeout.EventTimeTimeout)(updateAcrossEvents)
//      .writeStream
//      .queryName("deduplicated")
//      .outputMode("update")
//      .format("console")
//      .start()
//      .awaitTermination()
//  }
//
//  object DeserializerWrapper {
//    val deserializer = kafkaAvroDeserializer
//  }
//
//  class AvroDeserializer extends AbstractKafkaAvroDeserializer {
//    def this(client: SchemaRegistryClient) {
//      this()
//      this.schemaRegistry = client
//    }
//
//    override def deserialize(bytes: Array[Byte]): String = {
//      val genericRecord = super.deserialize(bytes).asInstanceOf[GenericRecord]
//      genericRecord.toString
//    }
//  }
//
//}
