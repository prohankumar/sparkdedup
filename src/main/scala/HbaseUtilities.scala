import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
import org.apache.hadoop.hbase.client.{ConnectionFactory, Get, Result, Scan, Table}
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp
import org.apache.hadoop.hbase.filter.{FilterList, SingleColumnValueFilter}
import org.apache.hadoop.hbase.util.Bytes

import scala.collection.mutable.Map
import scala.util.control.Breaks.{break, breakable}

class HbaseUtilities (zkUrl: String) extends Serializable {

  lazy val hConf = HBaseConfiguration.create();

  hConf.set("hbase.zookeeper.quorum", zkUrl)
  hConf.set("zookeeper.session.timeout", "20000")
  hConf.set("hbase.rpc.timeout", "20000")
  hConf.set("hbase.client.scanner.timeout.period", "20000")
  hConf.set("hbase.client.retries.number", "3")
  hConf.set("hbase.client.pause", "2000")
  hConf.set("zookeeper.recovery.retry", "3")

  lazy val connection = ConnectionFactory.createConnection(hConf)

  val tables: Map[String, Table] = Map()

  def getTable(table: String): Table =
  {
    if (tables.contains(table))
      tables(table)
    else {
      try {

        val tableName = TableName.valueOf(table)
        val admin = connection.getAdmin
        if (admin.tableExists(tableName)) {
          tables += (table -> connection.getTable(tableName))
          tables(table)
        }
        else {
          throw RuntimeException("Could not locate table %s in HBase".format(table))
        }

      }
      catch {
        case e: org.apache.hadoop.hbase.client.RetriesExhaustedException => {
          throw RuntimeException("Could not connect to HBase", e)
        }

        case e: org.apache.hadoop.hbase.TableNotFoundException => {
          //CHECKIT should we create the table if it does not exist?
          throw RuntimeException("Could not locate table %s in HBase".format(table), e)
        }
        case e: org.apache.hadoop.hbase.regionserver.NoSuchColumnFamilyException => {
          throw RuntimeException("Could not connect to HBase", e)
        }
      }
    }
  }


  def getRow(table: String, rowKey: String, columnFamily: String, columnName: String): Array[Byte] =
  {
    val g = new Get(Bytes.toBytes(rowKey))

    val tableObj = getTable(table)

    g.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))

    val result = tableObj.get(g)

    result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
  }

  def scanRowBestMatch(table: String, rowKey: String, columnFamily: String, columnName: String): Array[Byte] =
  {
    val tableObj = getTable(table)

    var output: Array[Byte] = null
    breakable {
      for (i <- Array(6, 5, 4)) {
        val scan = new Scan(Bytes.toBytes(rowKey))

        scan.setRowPrefixFilter(Bytes.toBytes(rowKey.substring(0, i)))

        scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))

        val scanner = tableObj.getScanner(scan)

        val result = scanner.next()

        if (result == null)
          output = null
        else {
          output = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
          break
        }

      }
    }

    output
  }

  def scanRowBestMatchMulti(table: String, bestMatchRowKey: String, rowKey: String, columnFamily: String, columnName: String): Array[Byte] =
  {
    val tableObj = getTable(table)

    var output: Array[Byte] = null
    breakable {
      for (i <- Range(0, bestMatchRowKey.length + 1).reverse) {

        val scan = new Scan(Bytes.toBytes(bestMatchRowKey + rowKey))
        scan.setRowPrefixFilter(Bytes.toBytes(bestMatchRowKey.substring(0, i) + rowKey))

        scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))

        val scanner = tableObj.getScanner(scan)

        val result = scanner.next()

        if (result == null)
          output = null
        else {
          output = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
          break
        }

      }
    }

    output
  }


  def scanRowBestMatchMulti2(table: String, bestMatchRowKey: String, columnFamily: String, columnName: String): Array[Byte] =
  {
    val tableObj = getTable(table)

    var output: Array[Byte] = null
    breakable {
      for (i <- Range(1, bestMatchRowKey.length + 1).reverse) {

        val g = new Get(Bytes.toBytes(bestMatchRowKey.substring(0, i)))
        val result: Result = tableObj.get(g)
        output = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))


        if (output == null) {
          output = null
        }
        else {
          break
        }

      }
    }

    output
  }

  def scanRowBestMatchMultiSpecific(table: String, bestMatchRowKey: String, subStringSizes: Array[Int], rowKey: String, columnFamily: String, columnName: String): Array[Byte] =
  {
    val tableObj = getTable(table)

    var output: Array[Byte] = null
    breakable {
      for (i <- subStringSizes) {
        val scan = new Scan(Bytes.toBytes(bestMatchRowKey + rowKey))

        scan.setRowPrefixFilter(Bytes.toBytes(bestMatchRowKey.substring(0, i) + rowKey))

        scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))

        val scanner = tableObj.getScanner(scan)

        val result = scanner.next()

        if (result == null)
          output = null
        else {
          output = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
          break
        }

      }
    }

    output
  }

  def scanRowBestMatchSpecific(table: String, rowKey: String, subStringSizes: Array[Int], columnFamily: String, columnName: String): Array[Byte] =
  {
    val tableObj = getTable(table)

    var output: Array[Byte] = null
    breakable {
      for (i <- subStringSizes) {
        val scan = new Scan(Bytes.toBytes(rowKey + rowKey))

        scan.setRowPrefixFilter(Bytes.toBytes(rowKey.substring(0, i) ))

        scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))

        val scanner = tableObj.getScanner(scan)

        val result = scanner.next()

        if (result == null)
          output = null
        else {
          output = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
          break
        }

      }
    }

    output
  }


  object tcmSpecificFunctions
  {
    def scanRowsCircleIdEventType(table: String, rowKey: String, columnFamily: String, columnName: String, circleId: String, eventType:String): Array[Byte] =
    {


      val tableObj = getTable(table)
      var output: Array[Byte] = null
      breakable {
        for (i <- Array(6, 5, 4)) {
          val scan = new Scan()

          scan.setRowPrefixFilter(Bytes.toBytes(rowKey.substring(0, i)))

          //val prefixFilter = new PrefixFilter(Bytes.toBytes(rowKey.substring(0, i)))
          val filter = new SingleColumnValueFilter(
            Bytes.toBytes(columnFamily),
            Bytes.toBytes("operator_code"),
            CompareOp.EQUAL,
            Bytes.toBytes(rowKey.substring(0, i))
          );

          val filter1 = new SingleColumnValueFilter(
            Bytes.toBytes(columnFamily),
            Bytes.toBytes("circle_id"),
            CompareOp.EQUAL,
            Bytes.toBytes(circleId)
          );

          val filter2 = new SingleColumnValueFilter(
            Bytes.toBytes(columnFamily),
            Bytes.toBytes("event_type"),
            CompareOp.EQUAL,
            Bytes.toBytes(eventType)
          );

          val filter3 = new SingleColumnValueFilter(
            Bytes.toBytes(columnFamily),
            Bytes.toBytes("ldca_type"),
            CompareOp.EQUAL,
            Bytes.toBytes("3")
          )

          val fList = new FilterList()

          fList.addFilter(filter)
          fList.addFilter(filter1)
          fList.addFilter(filter2)
          fList.addFilter(filter3)

          scan.setFilter(fList);

          scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
          scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("circle_id"))
          scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("event_type"))
          scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("ldca_type"))

          val scanner = tableObj.getScanner(scan)

          val result = scanner.next()

          if (result == null)
            output = null
          else
            output = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
        }
      }
      output
    }

    def scanRowsCircleIdBrand(table: String, rowKey: String, columnFamily: String, columnName: String, circleId: String, brand:String): Array[Byte] =
    {


      val tableObj = getTable(table)
      var output: Array[Byte] = null
      breakable {
        for (i <- Array(6, 5, 4)) {
          val scan = new Scan()
          scan.setRowPrefixFilter(Bytes.toBytes(rowKey.substring(0, i)))

          val filter = new SingleColumnValueFilter(
            Bytes.toBytes(columnFamily),
            Bytes.toBytes("operator_code"),
            CompareOp.EQUAL,
            Bytes.toBytes(rowKey.substring(0, i))
          );

          val filter1 = new SingleColumnValueFilter(
            Bytes.toBytes("cf"),
            Bytes.toBytes("circle_id"),
            CompareOp.EQUAL,
            Bytes.toBytes(circleId)
          );

          val filter2 = new SingleColumnValueFilter(
            Bytes.toBytes("cf"),
            Bytes.toBytes("brand_ind"),
            CompareOp.EQUAL,
            Bytes.toBytes(brand)
          );

          val fList = new FilterList()

          fList.addFilter(filter)
          fList.addFilter(filter1)
          fList.addFilter(filter2)

          scan.setFilter(fList);

          scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
          scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("circle_id"))
          scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes("brand_ind"))
          val scanner = tableObj.getScanner(scan)

          val result = scanner.next()

          if (result == null)
            output = null
          else
            output = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName))
        }
      }
      output
    }

  }

}
