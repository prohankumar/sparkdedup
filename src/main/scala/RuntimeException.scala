class RuntimeException (private val msg: String, private val cause: Throwable) extends Exception(msg, cause)

object RuntimeException
{
  def apply(msg: String, cause: Throwable = None.orNull): RuntimeException = new RuntimeException(msg, cause)
}


