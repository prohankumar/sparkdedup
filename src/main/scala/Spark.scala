//import io.confluent.kafka.schemaregistry.client.{CachedSchemaRegistryClient, SchemaRegistryClient}
//import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer
//import org.apache.avro.Schema
//import org.apache.avro.generic.GenericRecord
//import org.apache.spark.sql.{DataFrame, SparkSession}
//import org.apache.spark.sql.avro._
//import org.apache.spark.sql.expressions.Window
//import org.apache.spark.sql.functions._
//import org.apache.spark.sql.types.LongType
//
//
//object Spark {
//  private val schemaRegistryUrl = "http://127.0.0.1:30081"
//  private val kafkaUrl = "kafka.cluster.local:31090"
//
//  private val schemaRegistryClient = new CachedSchemaRegistryClient(schemaRegistryUrl, 128)
//  private val kafkaAvroDeserializer = new AvroDeserializer(schemaRegistryClient)
//
////  def upsertToDelta(microBatchOutputDF: DataFrame, batchId: Long) {
////
////    microBatchOutputDF
////      .select('smtUidNr, struct('msgTs, 'dcl, 'inv, 'evt, 'smt, 'msgInfSrcCd).as("cols"))
////      .groupBy('smtUidNr)
////      .agg(max('cols).as("latest"))
////      .select("smtUidNr", "latest.*")
////      .createOrReplaceTempView("updates")
////
////    microBatchOutputDF.sparkSession.sql(s"""
////    MERGE INTO raw t
////    USING updates s
////    ON (s.smtUidNr = t.smtUidNr and s.msgTs>t.msgTs)
////    WHEN MATCHED THEN UPDATE SET *
////    WHEN NOT MATCHED THEN INSERT *
////  """)
////  }
//
//
//  def main(args: Array[String]): Unit = {
//    val spark = SparkSession
//      .builder
//      .appName("ConfluentConsumer6")
//      .master("local[*]")
//      .getOrCreate()
//
//    spark.sparkContext.setLogLevel("ERROR")
//
//    spark.udf.register("deserialize", (bytes: Array[Byte]) =>
//      DeserializerWrapper.deserializer.deserialize(bytes)
//    )
//
//    val purchase2Topic = spark
//      .readStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaUrl)
//      .option("subscribe", "purchase10")
//      .option("startingOffsets", "earliest") // From starting
//      .load()
//
//    val purchaseDataFrame = purchase2Topic.selectExpr("""deserialize(value) AS message""")
//
//    val purchaseSchema = schemaRegistryClient.getLatestSchemaMetadata("purchase5" + "-value").getSchema
//    val purchaseSchemaSql = SchemaConverters.toSqlType(new Schema.Parser().parse(purchaseSchema))
//
//    val purchaseformattedDataFrame = purchaseDataFrame.select(
//      from_json(col("message"), purchaseSchemaSql.dataType).alias("purchase"))
//      .select("purchase.*")
//
//
//    val W  = Window.partitionBy(col("update_ts")).orderBy(lit(1))
//    val windowSpec = Window.partitionBy("user_id").orderBy(unix_timestamp(col("update_ts"), "yyyy-MM-dd'T'HH:mm:ss").cast("timestamp").desc)
//    val w = Window.partitionBy("user_id")
//
//
//
//    purchaseformattedDataFrame
//      .dropDuplicates("user_id", "bucket")
////      .select(
////        window(col("update_ts"), "5 seconds")
////      )
////      .withWatermark("window", "1 day")
////    .withColumn("rank", row_number().over(windowSpec))
////      .withColumn("window", window(col("update_ts"), "5 seconds").over(w))
////      .withColumn("test",  first("user_id").over(w))
//      .writeStream
////        .foreachBatch()
//      .queryName("deduplicated")
//      .format("console")
//      .option("truncate", false)
//      .start()
//      .awaitTermination()
//
////    purchaseformattedDataFrame
////      .withWatermark("update_ts", "5 seconds" )
////      .dropDuplicates("user_id")
////      .writeStream
////      .queryName("deduplicated")
////      .format("console")
////      .option("truncate", false)
////      .start()
////      .awaitTermination()
//
//
////
////      .format("console")
////      .option("truncate", false)
////      .start()
////      .awaitTermination()
//  }
//
//  object DeserializerWrapper {
//    val deserializer = kafkaAvroDeserializer
//  }
//
//  class AvroDeserializer extends AbstractKafkaAvroDeserializer {
//    def this(client: SchemaRegistryClient) {
//      this()
//      this.schemaRegistry = client
//    }
//
//    override def deserialize(bytes: Array[Byte]): String = {
//      val genericRecord = super.deserialize(bytes).asInstanceOf[GenericRecord]
//      genericRecord.toString
//    }
//  }
//
//}