//import io.confluent.kafka.schemaregistry.client.{CachedSchemaRegistryClient, SchemaRegistryClient}
//import io.confluent.kafka.serializers.AbstractKafkaAvroDeserializer
//import org.apache.avro.Schema
//import org.apache.avro.generic.GenericRecord
//import org.apache.spark.sql.{Dataset, Encoder, Encoders, SparkSession}
//import org.apache.spark.sql.avro.SchemaConverters
//import org.apache.spark.sql.functions._
//import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout, OutputMode}
//
//object Dedup {
//  private val schemaRegistryUrl = "http://127.0.0.1:30081"
//  private val kafkaUrl = "kafka.cluster.local:31090"
//
//  private val schemaRegistryClient = new CachedSchemaRegistryClient(schemaRegistryUrl, 128)
//  private val kafkaAvroDeserializer = new AvroDeserializer(schemaRegistryClient)
//
//
//  case class InputRow(user_id:Int, datetime:Long, purchase_value:String, product_type:String, update_ts:java.sql.Timestamp)
//  case class State(ids:Array[Int])
//
//
//  def main(args: Array[String]): Unit = {
//    val spark = SparkSession
//      .builder
//      .appName("ConfluentConsumer3")
//      .master("local[*]")
//      .getOrCreate()
//
//    spark.sparkContext.setLogLevel("ERROR")
//
//    spark.udf.register("deserialize", (bytes: Array[Byte]) =>
//      DeserializerWrapper.deserializer.deserialize(bytes)
//    )
//
//    val topicName = "purchase5"
//
//    val purchase2Topic = spark
//      .readStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaUrl)
//      .option("subscribe", topicName)
//      .option("startingOffsets", "earliest") // From starting
//      .load()
//
//    val purchaseDataFrame = purchase2Topic.selectExpr("""deserialize(value) AS message""")
//
//    val purchaseSchema = schemaRegistryClient.getLatestSchemaMetadata(topicName + "-value").getSchema
//    val purchaseSchemaSql = SchemaConverters.toSqlType(new Schema.Parser().parse(purchaseSchema))
//
//    val purchaseformattedDataFrame = purchaseDataFrame.select(
//      from_json(col("message"), purchaseSchemaSql.dataType).alias("purchase"))
//      .select("purchase.*")
//
//    import spark.implicits._
//
//
//    def updateStateWithEvent(state:State, input:InputRow):State = {
//      if (Option(input.user_id).isEmpty) {
//        return state
//      }
//      if (!state.ids.contains(input.user_id)) {
//        State(state.ids:+input.user_id)
//      } else {
//        state
//      }
//    }
//
//
//    def updateAcrossEvents(user_id:Int,
//                           inputs: Iterator[InputRow],
//                           oldState: GroupState[State]):State = {
////      if (oldState.hasTimedOut) {
////
////      }
//
//      var state:State = if (oldState.exists) oldState.get else State(Array())
//
//      for (input <- inputs) {
//        state = updateStateWithEvent(state, input)
//        oldState.update(state)
//      }
//      state
//    }
//
//
//    purchaseformattedDataFrame
//      .withWatermark("update_ts", "5 seconds")
//      .as[InputRow]
//      .groupByKey(_.user_id)
//      .mapGroupsWithState(GroupStateTimeout.EventTimeTimeout)(updateAcrossEvents)
//      .writeStream
//      .queryName("events")
//      .outputMode("update")
//      .format("console")
//      .start()
//      .awaitTermination()
//
//  }
//
//  object DeserializerWrapper {
//    val deserializer = kafkaAvroDeserializer
//  }
//
//  class AvroDeserializer extends AbstractKafkaAvroDeserializer {
//    def this(client: SchemaRegistryClient) {
//      this()
//      this.schemaRegistry = client
//    }
//
//    override def deserialize(bytes: Array[Byte]): String = {
//      val genericRecord = super.deserialize(bytes).asInstanceOf[GenericRecord]
//      genericRecord.toString
//    }
//  }
//
//}
