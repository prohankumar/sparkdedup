import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import org.apache.hadoop.hbase.util.Bytes
import org.apache.spark.sql.{SparkSession, functions}
import org.apache.spark.sql.functions.{coalesce, col, lit, udf}
import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout}
import org.apache.spark.sql.types.{FloatType, IntegerType, LongType, StringType}

object MMSC_IDEA {

//  TODO handle invalid name
case class InputRow(I_MSISDN: String, I_B_PARTY: String, I_A_PARTY_IMSI: String, I_B_PARTY_IMSI: String, I_START_TIMESTAMP: String, I_SENDER_TERMINAL: String, I_RECIPIENT_TERMINAL: String, I_MMSC_ID: String, I_CONTENT_TYPE: String, I_VIDEO_CONTENT_SIZE: String, I_VIDEO_CONTENT_NUMBER: String, I_AUDIO_CONTENT_SIZE: String, I_AUDIO_CONTENT_NUMBER: String, I_IMAGE_CONTENT_SIZE: String, I_IMAGE_CONTENT_NUMBER: String, I_TEXT_CONTENT_SIZE: String, I_TEXT_CONTENT_NUMBER: String, I_OTHER_CONTENT_SIZE: String, I_OTHER_CONTENT_NUMBER: String, I_DELIVERY_STATUS: String, I_ORIG_MSC: String, I_DEST_MSC: String, I_PRE_POST_INDICATOR: String, I_DIRECTION: String, I_EVENT_TYPE: String, I_S_P_NO_NETWORK: String, I_BEARERSERVICECODE: String, SRC_FILE_NAME: String, validity: String, inputRow: String)
case class InputRow2(I_MSISDN: String, I_B_PARTY: String, I_A_PARTY_IMSI: String, I_B_PARTY_IMSI: String, I_START_TIMESTAMP: String, I_SENDER_TERMINAL: String, I_RECIPIENT_TERMINAL: String, I_MMSC_ID: String, I_CONTENT_TYPE: String, I_VIDEO_CONTENT_SIZE: String, I_VIDEO_CONTENT_NUMBER: String, I_AUDIO_CONTENT_SIZE: String, I_AUDIO_CONTENT_NUMBER: String, I_IMAGE_CONTENT_SIZE: String, I_IMAGE_CONTENT_NUMBER: String, I_TEXT_CONTENT_SIZE: String, I_TEXT_CONTENT_NUMBER: String, I_OTHER_CONTENT_SIZE: String, I_OTHER_CONTENT_NUMBER: String, I_DELIVERY_STATUS: String, I_ORIG_MSC: String, I_DEST_MSC: String, I_PRE_POST_INDICATOR: String, I_DIRECTION: String, I_EVENT_TYPE: String, I_S_P_NO_NETWORK: String, I_BEARERSERVICECODE: String, SRC_FILE_NAME: String, validity: String, inputRow: String, bucket: java.sql.Timestamp)
//case class InputRow(I_MSISDN: String, I_B_PARTY: String, I_A_PARTY_IMSI: String, I_B_PARTY_IMSI: String, I_START_TIMESTAMP: String, I_SENDER_TERMINAL: String, I_RECIPIENT_TERMINAL: String, I_MMSC_ID: String, I_CONTENT_TYPE: String, I_VIDEO_CONTENT_SIZE: String, I_VIDEO_CONTENT_NUMBER: String, I_AUDIO_CONTENT_SIZE: String, I_AUDIO_CONTENT_NUMBER: String, I_IMAGE_CONTENT_SIZE: String, I_IMAGE_CONTENT_NUMBER: String, I_TEXT_CONTENT_SIZE: String, I_TEXT_CONTENT_NUMBER: String, I_OTHER_CONTENT_SIZE: String, I_OTHER_CONTENT_NUMBER: String, I_DELIVERY_STATUS: String, I_ORIG_MSC: String, I_DEST_MSC: String, I_PRE_POST_INDICATOR: String, I_DIRECTION: String, I_EVENT_TYPE: String, I_S_P_NO_NETWORK: String, I_BEARERSERVICECODE: String, validity: String, inputRow: String)

  case class OutputRow(bucket: java.sql.Timestamp, SUBS_CIRCLE_ID: Int, SUBS_MSISDN: String, EVENT_START_DATE: String, EVENT_START_TIME: String, EVENT_TYPE_KEY: Int, EVENT_CLASSIFICATION: Int, EVENT_DIRECTION: Int, PRE_POST_IND: Int, ROAMING_IND: Int, SENDER_TERMINAL: String, RECIPIENT_TERMINAL: String, CALLED_CALLING_NUMBER: String, MMSC_ID: String, SOURCE_NETWORK_OPERATOR_KEY: Int, ROAMING_NETWORK_OPERATOR_KEY: Int, CONTENT_TYPE: Int, VIDEO_CONTENT_SIZE: Int, VIDEO_CONTENT_NUMBER: Int, AUDIO_CONTENT_SIZE: Int, AUDIO_CONTENT_NUMBER: Int, IMAGE_CONTENT_SIZE: Int, IMAGE_CONTENT_NUMBER: Int, TEXT_CONTENT_SIZE: Int, TEXT_CONTENT_NUMBER: Int, OTHER_CONTENT_SIZE: Int, OTHER_CONTENT_NUMBER: Int, SUBS_MSISDN_LRN: Int, CALLED_CALLING_LRN: Int, SUBS_IMSI: String, CALLED_CALLING_IMSI: String, SUBS_MSISDN_PORT_FLAG: Int, CALLED_CALLING_PORT_FLAG: Int, DELIVERY_STATUS: String, S_P_NO_NETWORK: String, BEARER_SERVICE_CODE: String, CALL_TYPE: Int, TELE_SERVICE_CODE: String, VOLUME_VOLUME: Int, VOLUME_UMCODE: Int, SUBS_IMEI: String, S_P_LOCATION: String, NET_ELEMENT_NETWORK: String, RECORD_IDENTIFICATION_CDR_ID: String, USER_TYPE_CD: String, UPLINK_VOLUME: Long, DOWNLINK_VOLUME: Long, UPLINK_VOLUME_UMCODE: Int, DOWNLINK_VOLUME_UMCODE: Int, TOTAL_VOLUME: Long, CALL_GROSS_CHG_AMT: Int, CALL_DIALED_NUM: String, CELL_SITE_ID: String, NETWORK_TYPE_CD: String, SUBS_TYPE: String, SERVICE_NAME: String, OFFERING_ID_SVC: Int, LOAD_TIME_STAMP: String, BRAND_ID: String, REPORTED_DATE: String, SRC_FILE_NAME: String)
  case class State(data: InputRow2, dup: Int)


  def main(args: Array[String]): Unit = {

    val tcmIMSICodeSeriesLookup = "tcmIMSICodeSeriesLookup"
    val npdbMSISDNLookup = "npdbMSISDNLookup"
    val tcmLrnLookup = "tcmLrnLookup"
    val tcmOperatorCodeLookup = "tcmOperatorCodeLookup"
    val tcmLrnCircle = "tcmLrnCircle"
    val tcmOperatorCodeCircle = "tcmOperatorCodeCircle"


    if (args.length < 6) {
      print("Arguments not properly specified: [kafkaBrokers, topic, deduplicationInterval, checkpointPath, outputPath, batchSize, SourceCircleDimLocation, DiscoverDeliveryBearerDimLocation, SubsStatusDimLocation, contentTypeTable, productTypeTable]")
      System.exit(400)
    }

    //Command Line Args ************************************************************************
    val kafkaBrokers = args(0)
    val topic = args(1)
//    val watermark = args(2)
//    val checkpointPath = args(3)
//    val outputPath = args(4)
//    val batchSize = args(5)
//    val SourceCircleTable = args(6)
//    val DiscoverDeliveryBearerTable = args(7)
//    val SubsStatusTable = args(8)
//    val contentTypeTable = args(9)
//    val productTypeTable = args(10)
//    val rejectTopic = args(11)
    val security = args(2)
    val writePath = args(3)
    val zkUrl = args(4)
    val checkpointLocation = args(5)
    val table = args(6)
    //******************************************************************************************

    object HbaseObject{
      val table = new HbaseUtilities(zkUrl)
      table.getTable(tcmLrnLookup)
      table.getTable(tcmOperatorCodeLookup)
      table.getTable(tcmIMSICodeSeriesLookup)
      table.getTable(npdbMSISDNLookup)
      table.getTable(tcmLrnCircle)
      table.getTable(tcmOperatorCodeCircle)
    }

    val spark = SparkSession.builder().master("local[*]").appName("MMSC_IDEA").enableHiveSupport().getOrCreate()
    spark.sparkContext.setLogLevel("WARN")

    import spark.implicits._


//    val conf: Configuration = HBaseConfiguration.create
//
//    conf.addResource("core-site.xml")
//    conf.addResource("hbase-site.xml")
//    conf.addResource("hdfs-site.xml")


    //  TODO operator_circle=circle_id, execution_date = curr - 3
    val SUBS_CIRCLE_ID = udf((I_DIRECTION: Int, I_MSISDN: String, I_B_PARTY: String, I_A_PARTY_IMSI: String, I_B_PARTY_IMSI: String) => {
      var subsCircleId:Int = -99
      var SUBS_MSISDN:String = null
      var SUBS_IMSI:String = null


      if (I_DIRECTION == 2) {
        SUBS_MSISDN = I_MSISDN
        SUBS_IMSI = I_A_PARTY_IMSI
      } else {
        SUBS_MSISDN = I_B_PARTY
        SUBS_IMSI = I_B_PARTY_IMSI
      }
      if (SUBS_IMSI != null) {
        if (!SUBS_IMSI.startsWith("404") && !SUBS_IMSI.startsWith("405")) {
          subsCircleId = 100
        } else {
          val operatorCircle = HbaseObject.table.scanRowBestMatch(tcmIMSICodeSeriesLookup, SUBS_IMSI, "cf", "operator_circle")
          subsCircleId = Bytes.toString(operatorCircle).toInt
        }
      } else {
        val lrn = HbaseObject.table.getRow(npdbMSISDNLookup, SUBS_MSISDN, "cf", "route")
        if (lrn != null) {
          val operatorCircle = HbaseObject.table.getRow(tcmLrnLookup, Bytes.toString(lrn), "cf", "operator_circle")
          subsCircleId = Bytes.toString(operatorCircle).toInt
        }
        else {
          //          most likely 10 - 4
          val operatorCircle = HbaseObject.table.scanRowBestMatchMulti2(tcmOperatorCodeLookup, SUBS_MSISDN, "cf", "operator_circle")
          subsCircleId = Bytes.toString(operatorCircle).toInt
        }
      }
      subsCircleId
    })


    val SUBS_MSISDN = udf((I_DIRECTION: Int, I_MSISDN: String, I_B_PARTY: String) => {
      var subsMsisdn: String = null
      if (I_DIRECTION == 2) {
        subsMsisdn = I_MSISDN
      }
      else {
        subsMsisdn = I_B_PARTY
      }
      if (subsMsisdn.startsWith("91") && subsMsisdn.length>=12) {
        subsMsisdn= subsMsisdn.substring(2)
      } else if (subsMsisdn.startsWith("091") && subsMsisdn.length>=13) {
        subsMsisdn= subsMsisdn.substring(3)
      }
//      else {
//        subsMsisdn = "00" + subsMsisdn
//      }
      subsMsisdn
    })


    val EVENT_START_DATE = udf((START_TIME_TIMESTAMP: String) => {
      val dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
      val rtf = DateTimeFormatter.ofPattern("yyyy-MM-dd")
      try {
        rtf.format(dtf.parse(START_TIME_TIMESTAMP))
      }
      catch {
        case e: java.lang.ArrayIndexOutOfBoundsException => {
//          TODO handle error
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        }
        case e: java.time.format.DateTimeParseException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        }
      }
    })


    val EVENT_START_TIME = udf((START_TIME_TIMESTAMP: String) => {
      val dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
      val rtf = DateTimeFormatter.ofPattern("HH:mm:ss")
      try {
        rtf.format(dtf.parse(START_TIME_TIMESTAMP))
      }
      catch {
        case e: java.lang.ArrayIndexOutOfBoundsException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))
        }
        case e: java.time.format.DateTimeParseException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))
        }
      }
    })


    val EVENT_CLASSIFICATION = udf  ((CALLED_CALLING_LRN: Integer, CALLED_CALLING_NUMBER: String, SUBS_CIRCLE_ID: Int) => {
      var eventClassification: Int = 13
      var value: Array[Byte] = null
      var eventClsfType: Integer = null


      if (CALLED_CALLING_NUMBER.contains("@")) {
        eventClassification = 8
      } else {
        if (CALLED_CALLING_LRN != null) {
          value = HbaseObject.table.getRow(tcmLrnCircle, (CALLED_CALLING_LRN.toString + SUBS_CIRCLE_ID.toString), "cf", "EVENT_CLSF_TYPE")
          if (value != null) {
            eventClsfType = Bytes.toString(value).toInt
          }
        }

        else if (eventClsfType == null) {
          value = HbaseObject.table.scanRowBestMatchMulti(tcmOperatorCodeCircle, CALLED_CALLING_NUMBER ," " + SUBS_CIRCLE_ID.toString, "cf", "EVENT_CLSF_TYPE")
          if (value != null) {
            eventClsfType = Bytes.toString(value).toInt
          }
        }


        if (eventClsfType == null) {
          eventClassification = 13
        } else {
          if (eventClsfType == 2) {
            eventClassification = 8
          } else if (eventClsfType == 1) {
            eventClassification = 3
          } else if (eventClsfType == 3) {
            eventClassification = 7
          } else if (eventClsfType == 4) {
            eventClassification = 1
          } else if (eventClsfType == 5) {
            eventClassification = 2
          }
        }
      }


      eventClassification
    })


    val CALLED_CALLING_NUMBER = udf((I_DIRECTION: Int, I_MSISDN: String, I_B_PARTY: String) => {
      var calledCallingNumber: String = null
      if (I_DIRECTION == 2) {
        calledCallingNumber = I_B_PARTY
      }
      else {
        calledCallingNumber = I_MSISDN
      }

      if (calledCallingNumber.startsWith("91") && calledCallingNumber.length>=12) {
        calledCallingNumber= calledCallingNumber.substring(2)
      } else if (calledCallingNumber.startsWith("091") && calledCallingNumber.length>=13) {
        calledCallingNumber= calledCallingNumber.substring(3)
      }
//      else {
//        calledCallingNumber = "00" + calledCallingNumber
//      }
      calledCallingNumber
    })


    val SOURCE_NETWORK_OPERATOR_KEY = udf((CALLED_CALLING_NUMBER: String, CALLED_CALLING_LRN: String, SUBS_CIRCLE_ID: Int) => {
      var uniqueTrafficCodeID: Integer = -99
      if (CALLED_CALLING_NUMBER.contains("@")) {
        uniqueTrafficCodeID = -99
      } else {
        if (CALLED_CALLING_LRN != null && CALLED_CALLING_LRN != "") {
          val value = HbaseObject.table.getRow(tcmLrnCircle, CALLED_CALLING_LRN + SUBS_CIRCLE_ID, "cf", "UNIQUE_TRAFFIC_CODE_ID")
          if (value != null) {
            uniqueTrafficCodeID = Bytes.toString(value).toInt
          }
        } else {

          val value = HbaseObject.table.scanRowBestMatchMulti(tcmOperatorCodeCircle, CALLED_CALLING_NUMBER, " " + SUBS_CIRCLE_ID.toString, "cf", "UNIQUE_TRAFFIC_CODE_ID")
          if (value != null) {
            uniqueTrafficCodeID = Bytes.toString(value).toInt
          }
        }
      }
      uniqueTrafficCodeID
    })


    val SUBS_MSISDN_LRN = udf((SUBS_MSISDN: String) => {
      var subsMsisdnLrn: Integer = null

      if (SUBS_MSISDN.length() == 10) {
//        TODO check length of 10 lookup
        val lrn = HbaseObject.table.getRow(npdbMSISDNLookup, SUBS_MSISDN, "cf", "route")
        if (lrn != null) {
          subsMsisdnLrn = Bytes.toString(lrn).toInt
        }
      }
      else {
//        TODO should be null
        subsMsisdnLrn = null
      }
      subsMsisdnLrn
    })


    val CALLED_CALLING_LRN = udf((CALLED_CALLING_NUMBER: String) => {
      var calledCallingLrn: Integer = null

      if (CALLED_CALLING_NUMBER.length == 10) {
        val lrn = HbaseObject.table.getRow(npdbMSISDNLookup, CALLED_CALLING_NUMBER, "cf", "route")
        if (lrn != null) {
          calledCallingLrn = Bytes.toString(lrn).toInt
        }
      }
      else {
//        TODO should be null
        calledCallingLrn = null
      }
      calledCallingLrn
    })


    val SUBS_IMSI = udf((I_DIRECTION: Int, I_A_PARTY_IMSI: String, I_B_PARTY_IMSI: String) => {
      if (I_DIRECTION == 2) {
        I_A_PARTY_IMSI
      }
      else {
        I_B_PARTY_IMSI
      }
    })


    val CALLED_CALLING_IMSI = udf((I_DIRECTION: Int, I_A_PARTY_IMSI: String, I_B_PARTY_IMSI: String) => {
      if (I_DIRECTION == 2) {
        I_B_PARTY_IMSI
      }
      else {
        I_A_PARTY_IMSI
      }
    })


    val SUBS_MSISDN_PORT_FLAG = udf((SUBS_IMSI: String, SUBS_MSISDN: String, SUBS_MSISDN_LRN: Integer) => {
      var subsMsidnPortFlag: Int = 0



      if (SUBS_MSISDN != null && SUBS_MSISDN != "") {
        val value1 = HbaseObject.table.scanRowBestMatchMulti2(tcmOperatorCodeLookup, SUBS_MSISDN, "cf", "operator_name")
        var value2: Array[Byte] = null
        if (SUBS_MSISDN_LRN != null) {
          value2 = HbaseObject.table.getRow(tcmLrnLookup, SUBS_MSISDN_LRN.toString, "cf", "operator_name")
        }

        if (value2 == null) {
          value2 = HbaseObject.table.scanRowBestMatch(tcmIMSICodeSeriesLookup, SUBS_IMSI, "cf", "operator_name")
        }

        var operatorName1:String = null
        var operatorName2:String = null

        if (value1 != null) {
          operatorName1 = Bytes.toString(value1)
        }
        if (value2 != null) {
          operatorName2 = Bytes.toString(value2)
        }


        if (operatorName1 != null) {
          if (!(operatorName1.equalsIgnoreCase("Idea") || operatorName1.equalsIgnoreCase("VEL"))) {
            if (operatorName2 != null && (operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
              subsMsidnPortFlag = 1
            }
          } else {
            if (operatorName2 != null && !(operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
              subsMsidnPortFlag = 2
            }
          }
        }
      }
      subsMsidnPortFlag
    })


    val CALLED_CALLING_PORT_FLAG = udf((CALLED_CALLING_LRN: Integer, CALLED_CALLING_NUMBER: String, CALLED_CALLING_IMSI: String) => {
      var calledCallingPortFlag: Int = 0

//    TODO reject records when CALLED_CALLING_NUMBER is null
      if (CALLED_CALLING_NUMBER!= null && !CALLED_CALLING_NUMBER.contains("@")) {
        val value1 = HbaseObject.table.scanRowBestMatchMulti2(tcmOperatorCodeLookup, CALLED_CALLING_NUMBER, "cf", "operator_name")
        var value2: Array[Byte] = null
        if (CALLED_CALLING_LRN != null) {
          value2 = HbaseObject.table.getRow(tcmLrnLookup, CALLED_CALLING_LRN.toString, "cf", "operator_name")
        }


        if ((value2 == null) && (CALLED_CALLING_IMSI != null) && (!CALLED_CALLING_IMSI.equals(""))&& (!CALLED_CALLING_IMSI.equals("null"))) {
          value2 = HbaseObject.table.scanRowBestMatch(tcmIMSICodeSeriesLookup, CALLED_CALLING_IMSI, "cf", "operator_name")
        }

        var operatorName1:String = null
        var operatorName2:String = null
        if (value1 != null) {
          operatorName1 = Bytes.toString(value1)
        }
        if (value2 != null) {
          operatorName2 = Bytes.toString(value2)
        }

        if (operatorName1 != null) {
          if (!(operatorName1.equalsIgnoreCase("Idea") || operatorName1.equalsIgnoreCase("VEL"))) {
            if (operatorName2 != null && (operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
              calledCallingPortFlag = 1
            }
          } else {
            if (operatorName2 != null && !(operatorName2.equalsIgnoreCase("Idea") || operatorName2.equalsIgnoreCase("VEL"))) {
              calledCallingPortFlag = 2
            }
          }
        }
      }
      calledCallingPortFlag
    })


    //  TODO check, time stamp format
    val LOAD_TIME_STAMP = udf(() => {
      LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
    })



    // TODO input type, error msg
    val REPORTED_DATE = udf((fileName: String) => {
      val dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
      val rtf = DateTimeFormatter.ofPattern("yyyy-MM-dd")
      try {
        rtf.format(dtf.parse(fileName.split("_")(1)))
      }
      catch {
        case e: java.lang.ArrayIndexOutOfBoundsException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))

        }
        case e: java.time.format.DateTimeParseException => {
          LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))
        }
      }
    })


    val SRC_CIRCLE_ID = udf((SUBS_CIRCLE_ID: Int) => {
      SUBS_CIRCLE_ID
    })


    val BRAND_ID = udf((SRC_FILE_NAME: String) =>{
      if (SRC_FILE_NAME.startsWith("MMSPSM")) {
        1
      } else if (SRC_FILE_NAME.startsWith("MM") && (SRC_FILE_NAME(5).toString == "P" || SRC_FILE_NAME(5).toString == "R")) {
        2
      } else {
        3
      }
    })


    val inputDF = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaBrokers)
      .option("security.protocol", security)
      .option("startingOffsets", "earliest")
      .option("subscribe", topic)
      .load().selectExpr("CAST(value AS STRING)", "CAST(key AS STRING)")





    var parsedDF = inputDF.map( x => {
      val row = x.getString(0).split("\\|(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1).map(_.trim)
//      val row = x.getString(0).split(",", -1).map(_.trim)

//      val fileName = x.getString(1)

      try
        {
          InputRow(row(0), row(1), row(2), row(3), row(4), row(5), row(6), row(7), row(8), row(9), row(10), row(11), row(12), row(13), row(14), row(15), row(16), row(17), row(18), row(19), row(20), row(21), row(22), row(23), row(24), row(25), row(26), row(27), "valid", x.getString(0))
//          InputRow(row(0), row(1), row(2), row(3), row(4), row(5), row(6), row(7), row(8), row(9), row(10), row(11), row(12), row(13), row(14), row(15), row(16), row(17), row(18), row(19), row(20), row(21), row(22), row(23), row(24), row(25), row(26), "valid", x.getString(0))
        }
      catch
        {
          case e: java.lang.ArrayIndexOutOfBoundsException => InputRow("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "malformed record", x.getString(0))
//          case e: java.lang.ArrayIndexOutOfBoundsException => InputRow("", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "malformed record", x.getString(0))
        }
    }).toDF()










    parsedDF = parsedDF.withColumn("ts", functions.to_timestamp(col("I_START_TIMESTAMP"), "yyyyMMddHHmmss").cast("long"))

    def updateStateWithEvent(state:State, input:InputRow2):State = {

      if (Option(input.I_MSISDN).isEmpty) {
        return state
      }
      if (state.data==null) {
        State(input, 0)
      } else {
        State(input, 1)
      }
    }


    def updateAcrossEvents(tuple2: Tuple8[String, String, String, String, String, String, String, java.sql.Timestamp],
                           inputs: Iterator[InputRow2],
                           oldState: GroupState[State]):State = {
      //      if (oldState.hasTimedOut) {
      //
      //      }
      var state:State = if (oldState.exists) oldState.get else State(null, 0)

      for (input <- inputs) {
        state = updateStateWithEvent(state, input)
        oldState.update(state)
      }
      state
    }

    val bucketCol = functions.to_timestamp(col("ts").cast(LongType).minus(col("ts").cast(LongType).mod(259200)))

    parsedDF = parsedDF
      .withColumn("bucket", bucketCol)
      .withWatermark("bucket", "3 days")
      .as[InputRow2]
      .groupByKey(p=>(p.I_START_TIMESTAMP, p.I_DIRECTION, p.I_B_PARTY, p.I_MSISDN, p.I_IMAGE_CONTENT_SIZE, p.I_TEXT_CONTENT_SIZE, p.I_CONTENT_TYPE, p.bucket))
      .mapGroupsWithState(GroupStateTimeout.EventTimeTimeout)(updateAcrossEvents)
      .select("data.*")

//        parsedDF
//          .writeStream
//          .outputMode("update")
//          .format("console")
//          .start()
//          .awaitTermination()











    val directlyMappedColumns = Array( ("EVENT_TYPE_KEY", "I_EVENT_TYPE"), ("EVENT_DIRECTION", "I_DIRECTION"), ("PRE_POST_IND", "I_PRE_POST_INDICATOR"), ("SENDER_TERMINAL", "I_SENDER_TERMINAL"), ("RECIPIENT_TERMINAL", "I_RECIPIENT_TERMINAL"), ("MMSC_ID", "I_MMSC_ID"), ("CONTENT_TYPE", "I_CONTENT_TYPE"), ("VIDEO_CONTENT_SIZE", "I_VIDEO_CONTENT_SIZE"), ("VIDEO_CONTENT_NUMBER", "I_VIDEO_CONTENT_NUMBER"), ("AUDIO_CONTENT_SIZE", "I_AUDIO_CONTENT_SIZE"), ("AUDIO_CONTENT_NUMBER", "I_AUDIO_CONTENT_NUMBER"), ("IMAGE_CONTENT_SIZE", "I_IMAGE_CONTENT_SIZE"), ("IMAGE_CONTENT_NUMBER", "I_IMAGE_CONTENT_NUMBER"), ("TEXT_CONTENT_SIZE", "I_TEXT_CONTENT_SIZE"), ("TEXT_CONTENT_NUMBER", "I_TEXT_CONTENT_NUMBER"), ("OTHER_CONTENT_SIZE", "I_OTHER_CONTENT_SIZE"), ("OTHER_CONTENT_NUMBER", "I_OTHER_CONTENT_NUMBER"), ("DELIVERY_STATUS", "I_DELIVERY_STATUS"), ("S_P_NO_NETWORK", "I_S_P_NO_NETWORK"), ("BEARER_SERVICE_CODE", "I_BEARERSERVICECODE") )

    val columnsToType = Array( ("EVENT_DIRECTION","integer"), ("EVENT_TYPE_KEY", "integer"), ("PRE_POST_IND", "integer"), ("CONTENT_TYPE", "integer"), ("VIDEO_CONTENT_SIZE", "integer"), ("VIDEO_CONTENT_NUMBER", "integer"), ("AUDIO_CONTENT_SIZE", "integer"), ("AUDIO_CONTENT_NUMBER", "integer"), ("IMAGE_CONTENT_SIZE", "integer"), ("IMAGE_CONTENT_NUMBER", "integer"), ("TEXT_CONTENT_SIZE", "integer"), ("TEXT_CONTENT_NUMBER", "integer"), ("OTHER_CONTENT_SIZE", "integer"), ("OTHER_CONTENT_NUMBER", "integer"))

    val directRnamedDF = directlyMappedColumns.foldLeft(parsedDF.filter(col("validity") === "valid"))( (tempDF,column) => tempDF.withColumnRenamed(column._2, column._1))

    val directTypedDF = columnsToType.foldLeft(directRnamedDF)( (tempDF,column) => tempDF.withColumn(column._1, col(column._1).cast(column._2)))


//    val currentTimestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))


    val transformedDF = directTypedDF
      .withColumn("SUBS_CIRCLE_ID",SUBS_CIRCLE_ID(col("EVENT_DIRECTION"), col("I_MSISDN"), col("I_B_PARTY"), col("I_A_PARTY_IMSI"), col("I_B_PARTY_IMSI")))
      .withColumn("SUBS_MSISDN", SUBS_MSISDN(col("EVENT_DIRECTION"), col("I_MSISDN"), col("I_B_PARTY")))
      .withColumn("EVENT_START_DATE", EVENT_START_DATE(col("I_START_TIMESTAMP")))
      .withColumn("EVENT_START_TIME", EVENT_START_TIME(col("I_START_TIMESTAMP")))
      .withColumn("CALLED_CALLING_NUMBER", CALLED_CALLING_NUMBER(col("EVENT_DIRECTION"), col("I_MSISDN"), col("I_B_PARTY")))
      .withColumn("CALLED_CALLING_LRN", CALLED_CALLING_LRN(col("CALLED_CALLING_NUMBER")))
      .withColumn("EVENT_CLASSIFICATION", EVENT_CLASSIFICATION(col("CALLED_CALLING_LRN"), col("CALLED_CALLING_NUMBER"), col("SUBS_CIRCLE_ID")))
      .withColumn("ROAMING_IND", lit(1))
      .withColumn("CALLED_CALLING_IMSI", CALLED_CALLING_IMSI(col("EVENT_DIRECTION"), col("I_A_PARTY_IMSI"), col("I_B_PARTY_IMSI")))
      .withColumn("SOURCE_NETWORK_OPERATOR_KEY", SOURCE_NETWORK_OPERATOR_KEY(col("CALLED_CALLING_NUMBER"), col("CALLED_CALLING_LRN"), col("SUBS_CIRCLE_ID")))
      .withColumn("SUBS_MSISDN_LRN", SUBS_MSISDN_LRN(col("SUBS_MSISDN")))
      .withColumn("SUBS_IMSI", SUBS_IMSI(col("EVENT_DIRECTION"), col("I_A_PARTY_IMSI"), col("I_B_PARTY_IMSI")))
      .withColumn("SUBS_MSISDN_PORT_FLAG", SUBS_MSISDN_PORT_FLAG(col("SUBS_IMSI"), col("SUBS_MSISDN"), col("SUBS_MSISDN_LRN")))
      .withColumn("CALLED_CALLING_PORT_FLAG", CALLED_CALLING_PORT_FLAG(col("CALLED_CALLING_LRN"), col("CALLED_CALLING_NUMBER"), col("CALLED_CALLING_IMSI")))
      .withColumn("LOAD_TIME_STAMP", LOAD_TIME_STAMP())
      .withColumn("REPORTED_DATE", REPORTED_DATE(col("SRC_FILE_NAME")))
      .withColumn("SRC_CIRCLE_ID", SRC_CIRCLE_ID(col("SUBS_CIRCLE_ID")))
      .withColumn("BRAND_ID", BRAND_ID(col("SRC_FILE_NAME")))
//        .select("SUBS_CIRCLE_ID", "SUBS_MSISDN", "EVENT_START_DATE", "EVENT_START_TIME", "EVENT_TYPE_KEY", "EVENT_CLASSIFICATION", "EVENT_DIRECTION", "PRE_POST_IND", "ROAMING_IND", "SENDER_TERMINAL", "RECIPIENT_TERMINAL", "CALLED_CALLING_NUMBER", "MMSC_ID", "SOURCE_NETWORK_OPERATOR_KEY", "CONTENT_TYPE", "VIDEO_CONTENT_SIZE", "VIDEO_CONTENT_NUMBER", "AUDIO_CONTENT_SIZE", "AUDIO_CONTENT_NUMBER", "IMAGE_CONTENT_SIZE", "IMAGE_CONTENT_NUMBER", "TEXT_CONTENT_SIZE", "TEXT_CONTENT_NUMBER", "OTHER_CONTENT_SIZE", "OTHER_CONTENT_NUMBER", "SUBS_MSISDN_LRN", "CALLED_CALLING_LRN", "SUBS_IMSI", "CALLED_CALLING_IMSI", "SUBS_MSISDN_PORT_FLAG", "CALLED_CALLING_PORT_FLAG", "DELIVERY_STATUS", "S_P_NO_NETWORK", "BEARER_SERVICE_CODE", "LOAD_TIME_STAMP", "BRAND_ID", "REPORTED_DATE", "SRC_FILE_NAME")
      .na.fill(0, Seq("VIDEO_CONTENT_SIZE"))
      .na.fill(0, Seq("VIDEO_CONTENT_NUMBER"))
      .na.fill(0, Seq("AUDIO_CONTENT_SIZE"))
      .na.fill(0, Seq("AUDIO_CONTENT_NUMBER"))
      .na.fill(0, Seq("IMAGE_CONTENT_SIZE"))
      .na.fill(0, Seq("IMAGE_CONTENT_NUMBER"))
      .na.fill(0, Seq("TEXT_CONTENT_SIZE"))
      .na.fill(0, Seq("TEXT_CONTENT_NUMBER"))
      .na.fill(0, Seq("OTHER_CONTENT_SIZE"))
      .na.fill(0, Seq("OTHER_CONTENT_NUMBER"))
//    voda specific columns
        .withColumn("CALL_TYPE",lit(null).cast(IntegerType))
      .withColumn("TELE_SERVICE_CODE",  lit(null).cast(StringType))
        .withColumn("VOLUME_VOLUME", lit(null).cast(IntegerType))
        .withColumn("VOLUME_UMCODE", lit(null).cast(IntegerType))
        .withColumn("SUBS_IMEI", lit(null).cast(StringType))
        .withColumn("S_P_LOCATION", lit(null).cast(StringType))
        .withColumn("NET_ELEMENT_NETWORK", lit(null).cast(StringType))
        .withColumn("RECORD_IDENTIFICATION_CDR_ID", lit(null).cast(StringType))
        .withColumn("USER_TYPE_CD", lit(null).cast(StringType))
        .withColumn("UPLINK_VOLUME", lit(null).cast(LongType))
        .withColumn("DOWNLINK_VOLUME", lit(null).cast(LongType))
        .withColumn("UPLINK_VOLUME_UMCODE", lit(null).cast(IntegerType))
        .withColumn("DOWNLINK_VOLUME_UMCODE", lit(null).cast(IntegerType))
        .withColumn("TOTAL_VOLUME", lit(null).cast(LongType))
        .withColumn("CALL_GROSS_CHG_AMT", lit(null).cast(FloatType))
      .withColumn("CALL_DIALED_NUM", lit(null).cast(StringType))
      .withColumn("CELL_SITE_ID", lit(null).cast(StringType))
      .withColumn("NETWORK_TYPE_CD", lit(null).cast(StringType))
      .withColumn("SUBS_TYPE", lit(null).cast(StringType))
      .withColumn("SERVICE_NAME", lit(null).cast(StringType))
      .withColumn("OFFERING_ID_SVC", lit(null).cast(IntegerType))
      .withColumn("ROAMING_NETWORK_OPERATOR_KEY", lit(null).cast(IntegerType))
        .select("SUBS_CIRCLE_ID", "SUBS_MSISDN", "EVENT_START_DATE", "EVENT_START_TIME", "EVENT_TYPE_KEY", "EVENT_CLASSIFICATION", "EVENT_DIRECTION", "PRE_POST_IND", "ROAMING_IND", "SENDER_TERMINAL", "RECIPIENT_TERMINAL", "CALLED_CALLING_NUMBER", "MMSC_ID", "SOURCE_NETWORK_OPERATOR_KEY", "ROAMING_NETWORK_OPERATOR_KEY", "CONTENT_TYPE", "VIDEO_CONTENT_SIZE", "VIDEO_CONTENT_NUMBER", "AUDIO_CONTENT_SIZE", "AUDIO_CONTENT_NUMBER", "IMAGE_CONTENT_SIZE", "IMAGE_CONTENT_NUMBER", "TEXT_CONTENT_SIZE", "TEXT_CONTENT_NUMBER", "OTHER_CONTENT_SIZE", "OTHER_CONTENT_NUMBER", "SUBS_MSISDN_LRN", "CALLED_CALLING_LRN", "SUBS_IMSI", "CALLED_CALLING_IMSI", "SUBS_MSISDN_PORT_FLAG", "CALLED_CALLING_PORT_FLAG", "DELIVERY_STATUS", "S_P_NO_NETWORK", "BEARER_SERVICE_CODE", "CALL_TYPE", "TELE_SERVICE_CODE", "VOLUME_VOLUME", "VOLUME_UMCODE", "SUBS_IMEI", "S_P_LOCATION", "NET_ELEMENT_NETWORK", "RECORD_IDENTIFICATION_CDR_ID", "USER_TYPE_CD", "UPLINK_VOLUME", "DOWNLINK_VOLUME", "UPLINK_VOLUME_UMCODE", "DOWNLINK_VOLUME_UMCODE", "TOTAL_VOLUME", "CALL_GROSS_CHG_AMT", "CALL_DIALED_NUM", "CELL_SITE_ID", "NETWORK_TYPE_CD", "SUBS_TYPE", "SERVICE_NAME", "OFFERING_ID_SVC", "LOAD_TIME_STAMP", "BRAND_ID", "REPORTED_DATE", "SRC_FILE_NAME")



        transformedDF
//      .dropDuplicates("EVENT_START_DATE", "EVENT_START_TIME", "CALLED_CALLING_NUMBER", "IMAGE_CONTENT_SIZE", "TEXT_CONTENT_SIZE", "SUBS_MSISDN", "CONTENT_TYPE")
      .writeStream
          .format("com.hortonworks.spark.sql.hive.llap.streaming.HiveStreamingDataSource")
          .option("database", "test")
          .option("table", table)
          .option("metastoreUri", "thrift://svdg0784.ideaconnectprod.com:9083")
//          TODO check mode
          .outputMode("update")
//      .option("path", writePath)
      .option("checkpointLocation", checkpointLocation)

      .start()
        .awaitTermination()



//    transformedDF
//      .writeStream
//      .outputMode("update")
//      .format("console")
//      .start()
//      .awaitTermination()
//
//    transformedDF.show(10)


//      .select("GLOBAL_TRANSACTION_ID", "CIRCLE_ID", "ADJUSTMENT_CODE", "BILLING_KEYWORD", "DISCOVERY_BEARER", "DELIVERY_BEARER", "REVENUE_FLAG", "SERVICE_ID", "SERVICE_NAME", "SUBS_MSISDN", "SENDER_IMSI_A_PARTY", "GIFTED_PARTY_MSISDN", "URL", "SESSION_ID", "ROAMING_IDENTIFIER", "PORTAL_PARENT_TYPE", "PORTAL_PARENT_IDENTIFIER", "SUBCATEGORY", "LANGUAGE", "CONTENT_PROVIDER_CODE", "CONTENT_PROVIDER_NAME", "REVENUE_SHARE_PERCENTAGE", "REVENUE_VALUE", "PLATFORM_IDENTIFIER", "CONTENT_ITEM_CODE", "CONTENT_NAME", "CONTENT_TYPE", "CONTENT_TYPE_KEY", "MIME_TYPE", "CONTENT_HOST_TYPE", "CONTENT_INTERNAL_PRICE_ID", "CONTENT_DESCRIPTION", "DOWNLOAD_VOLUME_QUANTITY", "COPYRIGHT_ID", "DISCOUNT_VALUE", "DISCOUNT_PERCENTAGE", "DEVICE_MODEL_MAKE", "DEVICE_MODEL_NAME", "IMEI", "SUBSCRIPTION_FLAG", "SUBSCRIPTION_PACK_NAME", "CUSTOMER_SEGMENT", "PRE_POST_IND", "SUBS_STATUS_TYPE_KEY", "MNP_TYPE", "LRN", "UUP_RESPONSE_CODE", "UUP_RETRY_COUNT", "LOYALTY_POINTS_REDEEMED", "SERVICE_TYPE_ID", "TARIFF_CLASS", "CURRENCY", "BASE_PRICE_AMOUNT", "OPERATION_TYPE", "UCS_RETRY_COUNT", "UCS_RESPONSE_CODE", "ACTUAL_CHARGE_AMOUNT", "DELIVERY_STATUS_FLAG", "ERROR_CODE", "REQUEST_DATE", "REQUEST_TIME", "DELIVERY_DATE", "DELIVERY_TIME", "CAMPAIGN_ID", "CONTENT_PRODUCT_TYPE", "CONTENT_PRODUCT_TYPE_KEY", "ACTL_CHARG_AMT_WITH_ST", "ACTL_CHARG_ST_AMT", "BASE_LOYALTY_POINTS", "CSR_INITIATED", "ADDITIONAL_PARAMETER1", "ADDITIONAL_PARAMETER2", "ADDITIONAL_PARAMETER3", "ADDITIONAL_PARAMETER4", "ADDITIONAL_PARAMETER5", "ADDITIONAL_PARAMETER6", "ADDITIONAL_PARAMETER7", "ADDITIONAL_PARAMETER8", "ADDITIONAL_PARAMETER9", "ADDITIONAL_PARAMETER10", "BRAND_ID", "LOAD_TIMESTAMP", "SRC_FILE_NAME", "inputRow")

//    val nonNullablesColumns = Array ("GLOBAL_TRANSACTION_ID", "SERVICE_ID", "CONTENT_PROVIDER_CODE", "SUBS_KEY", "CHARGED_SUBS_MSISDN", "DELIVERY_DATE", "DELIVERY_TIME")
//
//    val nonNullablesFiltered = nonNullablesColumns.foldLeft(transformedDF)(  (tempDF, column) => tempDF.filter(col(column) =!= "").filter(col(column).isNotNull) ).drop("inputRow")
//    val nonNullablesRejected = nonNullablesColumns.foldLeft(transformedDF)(  (tempDF, column) => tempDF.filter(col(column) === "").filter(col(column).isNull).withColumn("validity", lit(column + " is null")) )
//
//
//    nonNullablesFiltered.withWatermark("watermarkDatetime", watermark + " days").dropDuplicates("GLOBAL_TRANSACTION_ID", "watermarkDatetime").drop("watermarkDatetime").repartition(5).writeStream.option("checkpointLocation", checkpointPath).outputMode("append").option("path",outputPath).trigger(Trigger.ProcessingTime(batchSize + " seconds")).format("orc").start()
//
//    val rejectedDF = parsedDF.filter(col("validity") =!= "valid")
//
//    rejectedDF
//      .select( col("SRC_FILE_NAME").alias("key"), to_json(struct("inputRow","validity")).alias("value"))
//      .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
//      .writeStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaBrokers)
//      .option("security.protocol", security)
//      .option("topic", rejectTopic)
//
//    nonNullablesRejected
//      .select( col("SRC_FILE_NAME").alias("key"), to_json(struct("inputRow","validity")).alias("value"))
//      .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
//      .writeStream
//      .format("kafka")
//      .option("kafka.bootstrap.servers", kafkaBrokers)
//      .option("security.protocol", security)
//      .option("topic", rejectTopic)

//    spark.streams.awaitAnyTermination()




  }


}
