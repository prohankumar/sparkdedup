//import java.time.format.DateTimeFormatter
//
//import org.apache.hadoop.conf.Configuration
//import org.apache.hadoop.hbase.{HBaseConfiguration, TableName}
//import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Get, Result, Table}
//import org.apache.hadoop.hbase.util.Bytes
//import org.apache.spark.sql.SparkSession
//import org.apache.spark.sql.functions._
//object MMSC_Voda {
//
//  def main(args: Array[String]): Unit = {
//    if (args.length < 11) {
//      print("Arguments not properly specified: [kafkaBrokers, topic, deduplicationInterval, checkpointPath, outputPath, batchSize, SourceCircleDimLocation, DiscoverDeliveryBearerDimLocation, SubsStatusDimLocation, contentTypeTable, productTypeTable]")
//      System.exit(400)
//    }
//
//    //Command Line Args ************************************************************************
//    val kafkaBrokers = args(0)
//    val topic = args(1)
//    val watermark = args(2)
//    val checkpointPath = args(3)
//    val outputPath = args(4)
//    val batchSize = args(5)
//    val SourceCircleTable = args(6)
//    val DiscoverDeliveryBearerTable = args(7)
//    val SubsStatusTable = args(8)
//    val contentTypeTable = args(9)
//    val productTypeTable = args(10)
//    val rejectTopic = args(11)
//    val security = args(12)
//    //******************************************************************************************
//
//    val spark = SparkSession.builder().master("local[*]").appName("MMSC_IDEA").getOrCreate()
//    spark.sparkContext.setLogLevel("WARN")
//
//    import spark.implicits._
//
//    case class InputRow(callType: String,startTime: String,teleServiceCode: String,bearerServiceCode: String,volumeVolume: String,volumeUmcode: String,spNumberAddress: String,spNumberNumberingPlan: String,spNumberTypeOfNumber: String,spNumberClir: String,spNumberNetwork: String,spPort: String,spEquipmentNumber: String,spEquipmentClassMark: String,spLocation: String,netElementAddress: String,netElementNetwork: String,opNumberAddress: String,opNumberTypeOfNumber: String,opNumberNumberingPlan: String,Rating_Group: String,routingNumberAddress: String,causeForForward: String,cugInfoCugInterLockCode: String,cugInfoCugInformation: String,cugInfoCugType: String,techInfoTermind: String,techInfoType: String,inTrunk: String,outTrunk: String,recordIdentificationCDRid: String,dataVolumeVolume: String,dataVolumeUmcode: String,messagesVolume: String,messagesUmcode: String,callTypeInfoXfileInd: String,sPNumberingPlan: String,spLocationNumberingPlan: String,routingNumberNumberingPlan: String,netElementNumberingPlan: String,upLinkVolumeVolume: String,downLinkVolumeVolume: String,upLinkVolumeUmcode: String,downLinkVolumeUmcode: String,startTimeDummy: String,rupees: String,servedPDPAddressAddress: String,Duration: String,CellID: String,TimeZone: String,rATType: String,GGSN_Address: String,GPRS_End_Time: String,CauseForRecClosing: String,NodeId: String,SubscriberTypeÿ: String,ContentType: String,ServiceName: String,f3gpp_Qos_Profile: String,Effective_Duration: String,PDP_Type: String,Selection_Mode: String,TimeOfFirstUsage: String,TimeOfLastUsage: String,DataPacketDownlink: String,DataPacketUplink: String,LocalSequence: String,DataVolDownlinkTTC: String,GGSN_MCC_MNC: String,AcctSessionID: String,Roaming: String, SRC_FILE_NAME: String, validity: String, inputRow: String)
//    case class InputRow2(callType: String, startTime: String, teleServiceCode: String, bearerServiceCode: String, volumeVolume: String, volumeUmcode: String, spNumberAddress: String, spNumberNumberingPlan: String, spNumberTypeOfNumber: String, spNumberClir: String, spNumberNetwork: String, spPort: String, spEquipmentNumber: String, spEquipmentClassMark: String, spLocation: String, netElementAddress: String, netElementNetwork: String, opNumberAddress: String, opNumberTypeOfNumber: String, opNumberNumberingPlan: String, RatingGroup: String, routingNumberAddress: String, causeForForward: String, cugInfoCugInterLockCode: String, cugInfoCugInformation: String, cugInfoCugType: String, techInfoTermind: String, techInfoType: String, inTrunk: String, outTrunk: String, recordIdentificationCDRid: String, dataVolumeVolume: String, dataVolumeUmcode: String, messagesVolume: String, messagesUmcode: String, callTypeInfoXfileInd: String, sPNumberingPlan: String, spLocationNumberingPlan: String, routingNumberNumberingPlan: String, netElementNumberingPlan: String, upLinkVolumeVolume: String, downLinkVolumeVolume: String, upLinkVolumeUmcode: String, downLinkVolumeUmcode: String, startTimeDummy: String, servedPDPAddressAddress: String, servedPDPAddressNumberingplan: String, recordIdentificationOrigCdrId: String, qualityOfServiceNegotiatedDelay: String, qualityOfServiceNegotiatedPeakThroughput: String, qualityOfserviceNegotiatedMeanThroughput: String, qualityOfserviceNegotiatedPrecedence: String, qualityOfserviceNegotiatedReliability: String, sgsnAddressSgsnAddress: String, netElementEntity: String, netElementAddressNew: String, netElementNumberingPlanNew: String, callTypeInfoNetworkInitContextId: String, technicalInfoAnonymousAccess: String, Duration: String, CellID: String, TimeZone: String, rATType: String, GGSN_Address: String, GPRSEndTime: String, CauseForRecClosing: String, NodeId: String, SubscriberType: String, ContentType: String, ServiceName: String, f3gpp_Qos_Profile: String, EffectiveDuration: String, PDP_Type: String, Selection_Mode: String, TimeOfFirstUsage: String, TimeOfLastUsage: String, DataPacketDownlink: String, DataPacketUplink: String, LocalSequence: String, DataVolDownlinkTTC: String, GGSN_MCC_MNC: String, AcctSessionID: String, Roaming: String, Delimiter: String, SRC_FILE_NAME: String, validity: String, inputRow: String)
//
//  }
//
//
//  val conf: Configuration = HBaseConfiguration.create
//
//  conf.addResource("core-site.xml")
//  conf.addResource("hbase-site.xml")
//  conf.addResource("hdfs-site.xml")
//
//  val connection: Connection = ConnectionFactory.createConnection(conf)
//  val npdb_msisdn_route: Table = connection.getTable(TableName.valueOf("npdb_msisdn_route"))
//
//  val spark = SparkSession
//    .builder
//    .appName("ConfluentConsumer6")
//    .master("local[*]")
//    .enableHiveSupport()
//    .getOrCreate()
//  import spark.implicits._
//  private val dim_plcode_mccmnc_master = spark.sql("select * from dim_plcode_mccmnc_master")
//
//
//
//  //  TODO doubt
//  private val SUBS_CIRCLE_ID = udf((spPort: String, spNumberAddress: String) => {
//    val tcm_lrn_operator_circle: Table = connection.getTable(TableName.valueOf("tcm_lrn_operator_circle"))
//    val tcm_operator_code_operator_circle: Table = connection.getTable(TableName.valueOf("tcm_operator_code_operator_circle"))
//
//    if (spPort != null) {
//      if (!spPort.startsWith("404") || !spPort.startsWith("405")) {
//        100
//      }
//      else {
//        val tcm_imsi_code_series_operator_circle: Table = connection.getTable(TableName.valueOf("tcm_imsi_code_series_operator_circle"))
//        val g = new Get(Bytes.toBytes(spPort))
//        val result: Result = tcm_imsi_code_series_operator_circle.get(g)
//        val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("operator_circle"))
//        Bytes.toInt(value)
//      }
//    }
//    else {
//      val g = new Get(Bytes.toBytes(spNumberAddress))
//      val result: Result = npdb_msisdn_route.get(g)
//      var value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("route"))
//      if (value != null) {
//        val g = new Get(value)
//        val result: Result = tcm_lrn_operator_circle.get(g)
//        value = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("operator_circle"))
//        Bytes.toInt(value)
//      }
//      else {
//        val g = new Get(Bytes.toBytes(spNumberAddress))
//        val result: Result = tcm_operator_code_operator_circle.get(g)
//        value = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("operator_circle"))
//        Bytes.toInt(value)
//      }
//    }
//  })
//
//  //  TODO input datatype
//  private val SUBS_MSISDN = udf((spNumberAddress: String) => {
//    if (spNumberAddress.startsWith("91") && spNumberAddress.length>=12) {
//      spNumberAddress.substring(2)
//    }
//    else {
//      "00" + spNumberAddress
//    }
//  })
//
//
//
//
//  private val EVENT_START_DATE = udf((startTime: String) => {
//    val dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
//    val rtf = DateTimeFormatter.ofPattern("yyyy-MM-dd")
//    try {
//      rtf.format(dtf.parse(startTime))
//    }
//    catch {
//      case e: java.lang.ArrayIndexOutOfBoundsException => {
//
//      }
//      case e: java.time.format.DateTimeParseException => {
//
//      }
//    }
//  })
//
//  private val EVENT_START_TIME = udf((startTime: String) => {
//    val dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss")
//    val rtf = DateTimeFormatter.ofPattern("HH:mm:ss")
//    try {
//      rtf.format(dtf.parse(startTime))
//    }
//    catch {
//      case e: java.lang.ArrayIndexOutOfBoundsException => {
//
//      }
//      case e: java.time.format.DateTimeParseException => {
//
//      }
//    }
//  })
//
//
//// TODO doubt
//  private val EVENT_TYPE_KEY = udf(() => {
//    4
//  })
//
//
//
//
//
////  TODO doubt
//  private val EVENT_CLASSIFICATION = udf((CALLED_CALLING_NUMBER: String, CALLED_CALLING_LRN: String) => {
//    if (CALLED_CALLING_NUMBER.contains("@")) {
//      8
//    }
//
//  })
//
//  private val EVENT_DIRECTION = udf(() => {
//    2
//  })
//
//
//  private val PRE_POST_IND = udf((fileName: String) => {
//    val ppi = fileName(6).toString
//    if (ppi.equals("P")) {
//      2
//    }
//    else if (ppi.equals("R")) {
//      1
//    }
//  })
//
//
//
//  private val ROAMING_IND = udf(() => {
//    1
//  })
//
//
//
//  private val CALLED_CALLING_LRN = udf((opNumberAddress: String) => {
//    if (opNumberAddress.length() == 10) {
//      val npdb_msisdn_route: Table = connection.getTable(TableName.valueOf("npdb_msisdn_route"))
//      val g = new Get(Bytes.toBytes(opNumberAddress))
//      val result: Result = npdb_msisdn_route.get(g)
//      val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("route"))
//      Bytes.toInt(value)
//    }
//    else {
//      null
//    }
//  })
//
//
//
//  // TODO input type
//  private val CALLED_CALLING_NUMBER = udf((opNumberAddress: String) => {
//    if (opNumberAddress.startsWith("91") && opNumberAddress.length>=12) {
//      opNumberAddress.substring(2)
//    }
//    else {
//      "00" + opNumberAddress
//    }
//  })
//
//
//
////  TODO use SRC_CIRCLE_ID
//  private val SOURCE_NETWORK_OPERATOR_KEY = udf((CALLED_CALLING_LRN: String, CALLED_CALLING_NUMBER: String, brand_id: Int, SRC_CIRCLE_ID: Int) => {
//    if (CALLED_CALLING_LRN != null) {
//      val tcm_lrn_unique_traffic_code_id: Table = connection.getTable(TableName.valueOf("tcm_lrn_unique_traffic_code_id"))
//      val g = new Get(Bytes.toBytes(CALLED_CALLING_LRN))
//      val result: Result = tcm_lrn_unique_traffic_code_id.get(g)
//      val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("unique_traffic_code_id"))
//      if (value == null) {
//        -99
//      }
//      else {
//        Bytes.toInt(value)
//      }
//    }
//    else {
//      if (CALLED_CALLING_NUMBER.length() < 10) {
//        val tcm_operator_code_circle_id_event_type_brand_ind_tcm_lrn_unique_traffic_code_id_ldca_type3: Table = connection.getTable(TableName.valueOf("tcm_operator_code_circle_id_event_type_brand_ind_tcm_lrn_unique_traffic_code_id_ldca_type3"))
//        val g = new Get(Bytes.toBytes(CALLED_CALLING_LRN))
//        val result: Result = tcm_operator_code_circle_id_event_type_brand_ind_tcm_lrn_unique_traffic_code_id_ldca_type3.get(g)
//        val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("unique_traffic_code_id"))
//        if (value == null) {
//          -99
//        }
//        else {
//          Bytes.toInt(value)
//        }
//      }
//      else {
//        if (brand_id == 1) {
//          val tcm_operator_code_circle_id_brand_ind_1_3_unique_traffic_code_id: Table = connection.getTable(TableName.valueOf("tcm_operator_code_circle_id_brand_ind_1_3_unique_traffic_code_id"))
//          val g = new Get(Bytes.toBytes(CALLED_CALLING_NUMBER))
//          val result: Result = tcm_operator_code_circle_id_brand_ind_1_3_unique_traffic_code_id.get(g)
//          val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("unique_traffic_code_id"))
//          if (value == null) {
//            -99
//          }
//          else {
//            Bytes.toInt(value)
//          }
//        }
//        else {
//          val tcm_operator_code_circle_id_brand_ind_2_3_unique_traffic_code_id: Table = connection.getTable(TableName.valueOf("tcm_operator_code_circle_id_brand_ind_2_3_unique_traffic_code_id"))
//          val g = new Get(Bytes.toBytes(CALLED_CALLING_NUMBER))
//          val result: Result = tcm_operator_code_circle_id_brand_ind_2_3_unique_traffic_code_id.get(g)
//          val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("unique_traffic_code_id"))
//          if (value == null) {
//            -99
//          }
//          else {
//            Bytes.toInt(value)
//          }
//
//        }
//      }
//    }
//  })
//
//
//  private val SUBS_MSISDN_LRN = udf((spNumberAddress: String) => {
//    if (spNumberAddress.length == 10) {
//      val npdb_msisdn_route: Table = connection.getTable(TableName.valueOf("npdb_msisdn_route"))
//      val g = new Get(Bytes.toBytes(spNumberAddress))
//      val result: Result = npdb_msisdn_route.get(g)
//      val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("route"))
//      Bytes.toInt(value)
//    }
//    else {
//      null
//    }
//  })
//
//  private val mccmnc_brand = dim_plcode_mccmnc_master.select(trim(col("mccmnc")),trim(col("brand"))).as[(String,Int)].collect().toMap.asInstanceOf[Map[String,Int]]
//
//
////  TODO best match, doubt
//  private val SUBS_MSISDN_PORT_FLAG = udf((SUBS_MSISDN_LRN: String, CALLED_CALLING_LRN: Int) => {
//    if (SUBS_MSISDN_LRN==null) {
//      0
//    }
//    else {
//      val tcm_operator_code_imsi_code_series: Table = connection.getTable(TableName.valueOf("tcm_operator_code_imsi_code_series"))
//      val g = new Get(Bytes.toBytes(CALLED_CALLING_LRN))
//      val result: Result = tcm_operator_code_imsi_code_series.get(g)
//      val value: Array[Byte] = result.getValue(Bytes.toBytes("cf"), Bytes.toBytes("unique_traffic_code_id"))
//      if (value != null) {
//        if (mccmnc_brand.get(Bytes.toString(value)) != null) {
//          0
//        }
//        else {
//          2
//        }
//      }
//      else {
////TODO
//      }
//
//
//
//    }
//  })
//
//
//  private val USER_TYPE_CD = udf((callTypeInfoXfileInd: String) => {
//    if (callTypeInfoXfileInd.equals("H")) {
//      "0"
//    }
//    else if (callTypeInfoXfileInd.equals("V")) {
//      "2"
//    }
//  })
//
//
//
//  private val TOTAL_VOLUME = udf((upLinkVolume: Long, downLinkVolume: Long) => {
//    upLinkVolume + downLinkVolume
//  })
//
//
//
//  private val CALL_GROSS_CHG_AMT = udf((CALL_GROSS_CHG_AMT: Double) => {
//    CALL_GROSS_CHG_AMT / 100.00
//  })
//
//
//  private val NETWORK_TYPE_CD = udf(() => {
//    "2G"
//  })
//
//
////  TODO condition for prepaid file, else clause
//  private val OFFERING_ID_SVC = udf((fileName: String, bearerServiceCode: Int) => {
//    if (fileName(5).toString.equals("R") && bearerServiceCode == 0) {
//      114
//    }
//    else {
//      null
//    }
//  })
//
//  private val BRAND = udf(() => {
//    2
//  })
//
//
//
//  private val SRC_CIRCLE_ID = udf((SUBS_CIRCLE_ID: Int) => {
//    SUBS_CIRCLE_ID
//  })
//}
