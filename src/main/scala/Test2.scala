import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.input_file_name

object Test2 {
  def main(args: Array[String]): Unit = {
    LogManager.getRootLogger.setLevel(Level.INFO)
    val spark = SparkSession.builder().master("local[*]").appName("Test2").getOrCreate()
    spark.sparkContext.setLogLevel("INFO")

    val df = spark
      .read
      .option("header", "false")
      .option("delimiter", "|")
      .csv("C:\\Users\\rohan\\Downloads\\MMSPSM1_20200714120000_25793575")
      .withColumn("filename", input_file_name)
    df.count()
  }

}
