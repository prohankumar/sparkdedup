//import java.text.SimpleDateFormat
//import java.util.Calendar
//
//import org.apache.hadoop.conf.Configuration
//import org.apache.hadoop.fs.{FileSystem, Path}
//import org.apache.spark.sql.SparkSession
//
//import scala.util.control.NonFatal
//import sys.process._
//
//object BL {
//  case class logTable(rectype: String, imsi_number: String, subno: String, int_flag: String, imei_number: String, b_subno: String, transdate: String, act_duration: String, lac: String, cell_id: String, switch: String, call_type: String, third_party_no: String, category: String, prepost_flag: String, route_out: String, route_in: String, file_name: String)
//  case class logTableID(ID: String, TD: java.sql.Timestamp, SUB: String, IMEI: String, CELL: String, BSUB: String, DUR: Int, CTYPE: String, IMSI: String, THIRD: String, R_IN: String, R_OUT: String, IN_FG: String, RTYPE: String, PP: String, SWT: String, CAT: String, LAC: String, FN: String)
//  case class fileDetails(filename: String, Status: Int)
//
//  def main(args: Array[String]): Unit = {
//    val spark = SparkSession
//      .builder
//      .appName("BL")
//      .master("local[*]")
//      .getOrCreate()
//
//    val raw_logs = spark.sparkContext.textFile("C:\\Users\\rohan\\Downloads\\NSS25_2006011200_3103A").coalesce(100, false)
//
//    val parsed_logs = raw_logs.filter(x => x.length() >= 821).map(x => {
//
//      			val y = x.substring(3, 6).trim()
//
//      			if (y == "001" || y == "002" || y == "029" || y == "030" || y == "031") {
//      				logTable(x.substring(3, 6).trim(), x.substring(9, 24).trim(), x.substring(25, 38).trim(), x.substring(38, 39).trim(), x.substring(41, 57).trim(), x.substring(124, 140).trim(), x.substring(179, 191).trim(), x.substring(191, 196).trim(), x.substring(235, 240).trim(), x.substring(241, 246).trim(), x.substring(247, 258).trim(), x.substring(300, 303).trim(), x.substring(326, 346).trim(), x.substring(336, 340).trim(), x.substring(349, 353).trim(), x.substring(420, 430).trim(), x.substring(433, 443).trim(), x.substring(800, 821).trim())
//      			}
//
//      			else {
//      				logTable("vxxx", x.substring(9, 24).trim(), x.substring(25, 38).trim(), x.substring(38, 39).trim(), x.substring(41, 57).trim(), x.substring(124, 140).trim(), x.substring(179, 191).trim(), x.substring(191, 196).trim(), x.substring(235, 240).trim(), x.substring(241, 246).trim(), x.substring(247, 258).trim(), x.substring(300, 303).trim(), x.substring(326, 346).trim(), x.substring(336, 340).trim(), x.substring(349, 353).trim(), x.substring(420, 430).trim(), x.substring(433, 443).trim(), x.substring(800, 821).trim())
//      			}
//
//      		})
////    parsed_logs.zipWithUniqueId().collect().foreach(println)
//
//
//      val IDRDD = parsed_logs.zipWithUniqueId()
//
//
//      val format = new SimpleDateFormat("yyMMddHHmmss")
//      //(rectype == '001') OR ( rectype == '002') OR ( rectype == '029') OR ( rectype == '030') OR ( rectype == '031' )) AND ( transdate != ''") AND (subno != ''")
//      val finalTable = IDRDD.filter(_._1.rectype != "vxxx").filter(_._1.transdate != "").filter(_._1.subno != "").map(x => {
//        val y = x._2 + 1
//
//        try {
//
//          val y_string = x._1.subno + x._1.transdate + f"$y%07d"
//
//          val parsed = format.parse(x._1.transdate)
//          val transdate = new java.sql.Timestamp(parsed.getTime())
//
//          transdate.setTime(transdate.getTime() + 21600000)
//
//          //case class logTableID(key: String, transdate: String, subno: String, imei_number: String, cell_id: String, b_subno: String, act_duration: String, call_type: String, imsi_number: String, third_party_no: String, route_in: String, route_out: String, int_flag: String, rectype: String, prepost_flag: String, switch: String, category: String, lac: String, file_name: String)
//          var duration = x._1.act_duration
//
//          if (x._1.act_duration == "") {
//            duration = "0"
//          }
//
//          logTableID(y_string, transdate, x._1.subno, x._1.imei_number, x._1.cell_id, x._1.b_subno, duration.toInt, x._1.call_type, x._1.imsi_number, x._1.third_party_no, x._1.route_in, x._1.route_out, x._1.int_flag, x._1.rectype, x._1.prepost_flag, x._1.switch, x._1.category, x._1.lac, x._1.file_name)
//        }
//        catch {
//          case NonFatal(t) => {
//            val y_string = x._1.subno + x._1.transdate + f"$y%07d"
//
//            val parsed = format.parse("160907152532126")
//            val transdate = new java.sql.Timestamp(parsed.getTime())
//
//            transdate.setTime(transdate.getTime() + 21600000)
//
//            //case class logTableID(key: String, transdate: String, subno: String, imei_number: String, cell_id: String, b_subno: String, act_duration: String, call_type: String, imsi_number: String, third_party_no: String, route_in: String, route_out: String, int_flag: String, rectype: String, prepost_flag: String, switch: String, category: String, lac: String, file_name: String)
//
//            logTableID("vxxx", transdate, x._1.subno, x._1.imei_number, x._1.cell_id, x._1.b_subno, "123".toInt, x._1.call_type, x._1.imsi_number, x._1.third_party_no, x._1.route_in, x._1.route_out, x._1.int_flag, x._1.rectype, x._1.prepost_flag, x._1.switch, x._1.category, x._1.lac, x._1.file_name)
//
//          }
//
//        }
//
//      }).filter(_.ID != "vxxx")
//
//    finalTable.collect().foreach(println)
////    val fileCountsRDD = finalTable.map(_.FN).countByValue()
////
////    val fileHead = fileCountsRDD.map(x=>{
////      val calendar = Calendar.getInstance()
////      val CurrentTimestamp = new java.sql.Timestamp(calendar.getTime().getTime())
////      CurrentTimestamp.setTime(CurrentTimestamp.getTime() + 21600000)
////
////      (x._1 +"A.gz", CurrentTimestamp, x._2)
////    })
//
//
////    spark.sparkContext.parallelize(fileHead.toList).collect().foreach(println)
//
//  }
//
//}
