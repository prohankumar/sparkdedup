//import java.text.SimpleDateFormat
//import java.time.OffsetDateTime
//import java.time.format.DateTimeFormatter
//import java.time.temporal.ChronoUnit
//
//import org.apache.spark.sql.functions.udf
//
//object Udfs {
//
//  //      TODO lookup
//  private val Subscriber_MSISDN = udf((MSISDN: String, CALL_DIRECTION: String, Subs_IMSI : String, Normalized_Called_Number: String) => {
//
//    if (CALL_DIRECTION == "O") {
//      if ((!Subs_IMSI.startsWith("404") || !Subs_IMSI.startsWith("405")) && !MSISDN.startsWith("00")) {
//        "00" + MSISDN
//      }
//      else {
//        if (MSISDN.startsWith("91")) {
//          MSISDN.drop(2)
//        } else if (MSISDN.startsWith("0")) {
//          MSISDN.drop(1)
//        }
//      }
//    }
//
//    else if (CALL_DIRECTION == "I") {
//      if ((!Subs_IMSI.startsWith("404") || !Subs_IMSI.startsWith("405")) && !Normalized_Called_Number .startsWith("00")) {
//        "00" + Normalized_Called_Number
//      }
//      else {
//        if (Normalized_Called_Number .startsWith("91")) {
//          Normalized_Called_Number .drop(2)
//        } else if (Normalized_Called_Number .startsWith("0")) {
//          Normalized_Called_Number .drop(1)
//        }
//      }
//    }
//
//  })
//
//
////  TODO Event Type
//  private val Called_Calling_Number = udf((Dialled_Number: String, Event_Type: String, CALL_DIRECTION  : String, MSISDN: String) => {
//
//    if (Event_Type.equals("Voice") || Event_Type.equals("SMS")) {
//      if (CALL_DIRECTION.equals("O")) {
//        if (Dialled_Number.startsWith("91")) {
//          Dialled_Number.drop(2)
//        } else if (Dialled_Number.startsWith("0")) {
//          Dialled_Number.drop(1)
//        }
//      }
//      else if (CALL_DIRECTION.equals("I")) {
//        if (MSISDN.startsWith("91")) {
//          MSISDN.drop(2)
//        } else if (MSISDN.startsWith("0")) {
//          MSISDN.drop(1)
//        }
//      }
//      else {
//        "-99"
//      }
//    }
//  })
//
//
//
////  TODO lookup table
//  private val Source_Circle_Id = udf(() => {
//
//  })
//
//  //  TODO lookup table
//  private val Subscriber_Circle_Id = udf(() => {
//
//  })
//
//  //  TODO lookup table
//  private val Event_Type_Key = udf((SERVICE_TYPE: String) => {
//    if (SERVICE_TYPE == "V") {
//      "1"
//    }
//    else if (SERVICE_TYPE == "S") {
//      "2"
//    }
//    else if (SERVICE_TYPE == "G") {
//      "5"
//    }
//    else {
//      "-99"
//    }
//  })
//
//  //  TODO lookup table
//  private val Event_Type_Classification_Key = udf(() => {
//
//  })
//
//
//
//  //  TODO lookup table
//  private val Event_Direction = udf((CALL_DIRECTION : String) => {
//    if (CALL_DIRECTION == "I") {
//      "1"
//    }
//    else if (CALL_DIRECTION == "O") {
//      "2"
//    }
//    else {
//      "-99"
//    }
//  })
//
//  //  TODO lookup table
//  private val ICR_FLAG_BI = udf(() => {
//
//  })
//
//
//  //  TODO lookup table
//  private val ROAMING_IND = udf(() => {
//
//  })
//
//
////  TODO Start_Event_Date_time format, outputFormat
//  private val event_Start_Date = udf((Start_Event_Date_time: String) => {
//    val inputFormat  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//    val outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
//    outputFormat.format(inputFormat.parse(Start_Event_Date_time))
//  })
//
//
//  //  TODO Start_Event_Date_time format, outputFormat
//  private val event_Start_Time = udf((Start_Event_Date_time: String) => {
//    val inputFormat  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//    val outputFormat = DateTimeFormatter.ofPattern("HH:mm:ss")
//    outputFormat.format(inputFormat.parse(Start_Event_Date_time))
//  })
//
//
//  //  TODO Start_Event_Date_time format, outputFormat
//  private val event_End_Date = udf((End_Event_Date_time: String) => {
//    val inputFormat  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//    val outputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
//    outputFormat.format(inputFormat.parse(End_Event_Date_time))
//  })
//
//
//  //  TODO Start_Event_Date_time format, outputFormat
//  private val event_End_Time = udf((End_Event_Date_time: String) => {
//    val inputFormat  = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
//    val outputFormat = DateTimeFormatter.ofPattern("HH:mm:ss")
//    outputFormat.format(inputFormat.parse(End_Event_Date_time))
//  })
//
//
////  TODO output data type
//  private val Event_Duration = udf((SERVICE_TYPE : String, End_Event_Date_time: String, Start_Event_Date_time: String ) => {
//    if (SERVICE_TYPE.equals("V") || SERVICE_TYPE.equals("G")) {
//      val endTime = OffsetDateTime.parse(End_Event_Date_time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
//      val startTime = OffsetDateTime.parse(Start_Event_Date_time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
//      startTime.until(endTime, ChronoUnit.SECONDS).toString
//    }
//    else {
//      "0"
//    }
//  })
//
//
//  //  TODO lookup table
//  private val SOURCE_NETWORK_OPERATOR_KEY = udf(() => {
//
//  })
//
//
//
//  //  TODO lookup table
//  private val Roaming_Network_Operator_Key = udf(() => {
//
//  })
//
//
////  TODO direct mapping with defaults
//
//
//
//  private val APN_ID = udf((SERVICE_TYPE: String, GPRSDestination_APN_NI:String) => {
//    if (SERVICE_TYPE.equals("G")) {
//      GPRSDestination_APN_NI
//    }
//    else {
//      "-99"
//    }
//  })
//
//
//
//  //  TODO lookup table
//  private val BRAND_ID_BI = udf(() => {
//
//  })
//
//
////  TODO doubt
//  private val FIRST_CGI_IF = udf(() => {
//
//  })
//
//
////  TODO input and output types, load_date
//  private val REPORTED_DATE = udf((Load_time: String, Event_start_Date: String) => {
//    val load_time = OffsetDateTime.parse(Load_time, DateTimeFormatter.ofPattern("HH:mm:ss"))
//    val event_start_date = OffsetDateTime.parse(Event_start_Date  , DateTimeFormatter.ofPattern("yyyy-MM-dd"))
//    val load_date = OffsetDateTime.now()
//    if ((load_time.compareTo(OffsetDateTime.parse("03:30:00", DateTimeFormatter.ofPattern("HH:mm:ss"))) == 0 || load_time.compareTo(OffsetDateTime.parse("03:30:00", DateTimeFormatter.ofPattern("HH:mm:ss"))) == -1) && event_start_date.compareTo(load_date) == -1) {
//      load_date.minusDays(1)
//    } else {
//      load_date
//    }
//  })
//
//
////  TODO loading time of record in hadoop
//  private val LOAD_TIMESTAMP = udf(() => {
//
//  })
//
//
////  TODO This fileld indicates unique input file id.
//  private val SRC_FILE_NAME = udf(() => {
//
//  })
//}
//
//
