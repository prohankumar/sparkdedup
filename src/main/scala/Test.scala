import org.apache.spark.sql.SparkSession

object Test {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession.builder().master("local[*]").appName("Test").enableHiveSupport().getOrCreate()

    val kafkaBrokers = args(0)
    val topic = args(1)
    //    val watermark = args(2)
    //    val checkpointPath = args(3)
    //    val outputPath = args(4)
    //    val batchSize = args(5)
    //    val SourceCircleTable = args(6)
    //    val DiscoverDeliveryBearerTable = args(7)
    //    val SubsStatusTable = args(8)
    //    val contentTypeTable = args(9)
    //    val productTypeTable = args(10)
    //    val rejectTopic = args(11)
    val security = args(2)
    val table = args(3)
    val checkpointLocation = args(4)
//    val checkpointLocation = args(5)


    val inputDF = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafkaBrokers)
      .option("security.protocol", security)
      .option("startingOffsets", "earliest")
      .option("subscribe", topic)
      .load().selectExpr("CAST(value AS STRING)", "CAST(key AS STRING)")

//    inputDF.writeStream.outputMode("update")
//          .format("console")
//          .start()
//          .awaitTermination()


//    inputDF.createOrReplaceTempView("mytempTable")
//    spark.sqlContext.sql("create table "+table+ " as select * from mytempTable")
    inputDF.writeStream.format("com.hortonworks.spark.sql.hive.llap.streaming.HiveStreamingDataSource")
      .option("database", "test")
      .option("table", table)
      .option("checkpointLocation", checkpointLocation)
      .option("metastoreUri", "thrift://svdg0784.ideaconnectprod.com:9083")
      .outputMode("update")
      .start()

    spark.streams.awaitAnyTermination()

  }
}
