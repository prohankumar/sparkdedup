name := "spark"
version := "0.2"
scalaVersion := "2.11.8"
Global / onChangedBuildSource := ReloadOnSourceChanges
//val sparkVersion = "2.4.5"
//val confluentVersion ="5.5.0"

val scala_tools_version = "2.11"
val spark_version = "2.3.1"
val scala_version = "2.11.8"

resolvers += "Spark Packages Repo" at "https://dl.bintray.com/spark-packages/maven"
resolvers += "Hortonworks repo" at "http://repo.hortonworks.com/content/repositories/releases/"


//resolvers += "confluent" at "https://packages.confluent.io/maven/"

//libraryDependencies += "org.apache.avro" % "avro" % "1.9.2"
//libraryDependencies += "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.10.2"

//libraryDependencies += "io.confluent" % "kafka-schema-registry-client" % confluentVersion
//libraryDependencies += "io.confluent" % "kafka-avro-serializer" % confluentVersion
//libraryDependencies += "za.co.absa" %% "abris" % "3.2.0"
//libraryDependencies += "org.json" % "json" % "20200518"
//dependencyOverrides += "com.google.guava" % "guava" % "15.0"
//libraryDependencies += "org.apache.hbase" % "hbase-client" % "2.3.0"

//libraryDependencies ++= Seq(
//  "org.apache.spark" %% "spark-core" % sparkVersion,
//  "org.apache.spark" %% "spark-sql" % sparkVersion,
//  "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
////  "org.apache.spark" %% "spark-avro" % sparkVersion
//)



val `spark-core` = "org.apache.spark" % ("spark-core_" + scala_tools_version) % spark_version
val `spark-sql` = "org.apache.spark" % ("spark-sql_" + scala_tools_version) % spark_version
val `spark-sql-kafka-0-10` = "org.apache.spark" % ("spark-sql-kafka-0-10_" + scala_tools_version) % spark_version
val `hbase-client` = "org.apache.hbase" % "hbase-client" % "1.3.5"

libraryDependencies += "com.hortonworks.hive" %% "hive-warehouse-connector" % "1.0.0.3.1.5.39-3"

libraryDependencies ++= Seq(`spark-core`, `spark-sql`,  `spark-sql-kafka-0-10`, `hbase-client`)

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false, includeDependency = false)
assemblyMergeStrategy in assembly :=
  {
    case PathList("META-INF", xs @ _*) => MergeStrategy.discard
    case x => MergeStrategy.first
  }